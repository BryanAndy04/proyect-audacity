-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para senda
CREATE DATABASE IF NOT EXISTS `senda` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `senda`;

-- Volcando estructura para tabla senda.caracteristicas
CREATE TABLE IF NOT EXISTS `caracteristicas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `brochure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vistapanoramica` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.caracteristicas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `caracteristicas` DISABLE KEYS */;
REPLACE INTO `caracteristicas` (`id`, `brochure`, `vistapanoramica`, `created_at`, `updated_at`) VALUES
	(1, 'brochure/159132685328099.pdf', '<iframe src="https://www.youtube.com/embed/IJQKs6LdxYs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>', NULL, '2020-06-05 03:35:21');
/*!40000 ALTER TABLE `caracteristicas` ENABLE KEYS */;

-- Volcando estructura para tabla senda.descripcion
CREATE TABLE IF NOT EXISTS `descripcion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_loft_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_duo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_social_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exp_area_com_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.descripcion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `descripcion` DISABLE KEYS */;
REPLACE INTO `descripcion` (`id`, `link_video`, `exp_loft_url`, `exp_duo_url`, `exp_social_url`, `exp_area_com_url`, `created_at`, `updated_at`) VALUES
	(1, '<iframe  src="https://www.youtube.com/embed/IJQKs6LdxYs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>', 'https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/', 'https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/', '-', '-', NULL, '2020-06-05 03:12:44');
/*!40000 ALTER TABLE `descripcion` ENABLE KEYS */;

-- Volcando estructura para tabla senda.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla senda.inicio
CREATE TABLE IF NOT EXISTS `inicio` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_personalizado` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_subtitulo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ruta_imagen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.inicio: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `inicio` DISABLE KEYS */;
REPLACE INTO `inicio` (`id`, `title`, `title_personalizado`, `tipo_subtitulo`, `subtitulo`, `ruta_imagen`, `orden`, `estado`, `created_at`, `updated_at`) VALUES
	(1, 'TODA GRAN VIDA REQUIERE', 'AUDACITY', 'direccion', 'Visita departamento piloto AV. Javier Prado este no 1501', 'inicio/159131267922180.png', '1', '1', '2020-06-04 23:15:18', '2020-06-04 23:17:59'),
	(2, 'TE ELEVA A OTRA', 'ALTURA', 'descripcion', 'Un desarrollo que te eleva a otra altura, la verticalización del futuro.', 'inicio/159131259276011.png', '2', '1', '2020-06-04 23:16:32', '2020-06-04 23:16:32'),
	(3, 'DESARROLLO EN TODAS LAS', 'NUEVAS ALTURAS', 'descripcion', '', 'inicio/159131271963369.png', '3', '1', '2020-06-04 23:18:39', '2020-06-04 23:18:39');
/*!40000 ALTER TABLE `inicio` ENABLE KEYS */;

-- Volcando estructura para tabla senda.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.migrations: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(47, '2014_10_12_000000_create_users_table', 1),
	(48, '2014_10_12_100000_create_password_resets_table', 1),
	(49, '2019_08_19_000000_create_failed_jobs_table', 1),
	(50, '2020_06_02_234308_inicio', 1),
	(51, '2020_06_04_132531_create_descripcion_table', 1),
	(52, '2020_06_04_201558_create_caracteristicas_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla senda.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla senda.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla senda.users: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin Admin', 'admin@argon.com', '2020-06-04 20:21:15', '$2y$10$SrEryDrP.n31lS2XRPE3ze28sX9i3IKmOnAg2Q4eV69WapdzJol02', NULL, '2020-06-04 20:21:15', '2020-06-04 20:21:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
