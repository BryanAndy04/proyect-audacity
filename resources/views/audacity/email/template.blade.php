<html>

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>

<body>
    <span class='CodTipoDocumentoEVOLTA' hidden="true"> {{ $data['tipodocumento'] }}</span><br />
    <span>N° Documento: </span> <span class='NroDocumentoEVOLTA'> {{ $data['numerodocumento'] }} </span><br />
    <span>Nombres: </span> <span class='NombreEVOLTA'>{{ $data['nombre'] }}</span><br />
    <span>Apellidos: </span> <span class='ApellidoEVOLTA'>{{ $data['apellidos'] }}</span><br />
    <span>Correo: </span> <span class='CorreoEVOLTA'>{{ $data['email'] }}</span><br />
    <span>Telefono: </span> <span class='TelefonoEVOLTA'>{{ $data['telefono'] }}</span><br />
    <span>Comentarios: </span><br></br>
    <span class='ComentarioEVOLTA'>

        <b style="font-size: 20px !important;">Autorización </b> <br>

        <b>Autorizo a que me envíen publicidad y/o promociones.<b> : @if( $data['publi_promociones'] == '1' )
                Si. @else No. @endif <br>
                <b>Autorizo que mi información sea compartida con el resto de empresas del grupo.<b> : @if(
                        $data['info_promociones'] == '1' ) Si. @else No. @endif <br><br>

                        <b style="font-size: 20px !important;">Cotización </br> <br>

                            <b>Tipo de departameto seleccionado</> : {{ $data['tipodepartamento'] }}



    </span>
    <span>Utm Campaign: </span> <span class='UtmCampaignEVOLTA'> Campaign </span><br />
    <span>Utm Content: </span> <span class='UtmContentEVOLTA'> Content </span><br />
    <span>Utm Medium: </span> <span class='UtmMediumEVOLTA'> Medium </span><br />
    <span>Utm Source: </span> <span class='UtmSourceEVOLTA'> Source </span><br />
    <span>Utm Term: </span> <span class='UtmTermEVOLTA'> Term </span><br />
</body>

</html>
