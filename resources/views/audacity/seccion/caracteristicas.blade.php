<section class="caracteristicas" id="caracteristicas">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4 d-flex align-items-center">
                <div class="">
                    <h2 class="titulo titulo--grande" data-aos="fade-right" data-aos-easing="ease">Un lugar ideal
                        <br> para vivir</h2>
                    <div class="texto" data-aos="fade-right" data-aos-easing="ease">
                        <p class="parrafo">Audacity, es unos de los mejores lugares certificado por Grade place to
                            live. Donde cada detalle es parte de un nuevo ecosistema para vivir mejor, disfrutar y
                            ahorrar.</p>
                    </div>
                    <div class="boton__pdf" data-aos="fade-up" data-aos-easing="ease">
                        <a href="{{ url('storage') }}/{{ $caracteristicas->brochure }}" class="buttom buttom__border" target="_blank">
                            <svg width="21" height="27" viewBox="0 0 21 27" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M19.8262 5.64179L15.4564 1.33698C14.9876 0.875201 14.3522 0.613525 13.6908 0.613525H3.06055C1.68034 0.618656 0.560547 1.7218 0.560547 3.08148V24.426C0.560547 25.7857 1.68034 26.8888 3.06055 26.8888H18.0605C19.4408 26.8888 20.5605 25.7857 20.5605 24.426V7.3863C20.5605 6.73467 20.2949 6.10357 19.8262 5.64179ZM17.8574 7.18619H13.8939V3.28159L17.8574 7.18619ZM3.06055 24.426V3.08148H11.3939V8.4176C11.3939 9.10001 11.9512 9.64902 12.6439 9.64902H18.0605V24.426H3.06055ZM16.0918 17.0529C15.4564 16.4372 13.6439 16.6065 12.7376 16.7194C11.8418 16.1806 11.2428 15.4367 10.821 14.3438C11.0241 13.5177 11.347 12.2606 11.1022 11.4705C10.8835 10.1262 9.13346 10.2596 8.88346 11.1678C8.6543 11.9938 8.86263 13.1432 9.24805 14.6106C8.72721 15.8369 7.95117 17.4839 7.4043 18.428C6.36263 18.9564 4.95638 19.7723 4.74805 20.7984C4.57617 21.6091 6.10221 23.6307 8.71159 19.1976C9.87825 18.8179 11.1491 18.351 12.2741 18.1663C13.2585 18.6896 14.4095 19.0385 15.1803 19.0385C16.5085 19.0385 16.6387 17.5916 16.0918 17.0529ZM5.77409 21.0447C6.03971 20.3418 7.05013 19.5311 7.35742 19.2489C6.36784 20.8036 5.77409 21.0806 5.77409 21.0447ZM10.0241 11.2652C10.4095 11.2652 10.373 12.9123 10.1178 13.3586C9.88867 12.6455 9.89388 11.2652 10.0241 11.2652ZM8.75325 18.274C9.25846 17.4069 9.69075 16.3756 10.0397 15.4674C10.472 16.2422 11.0241 16.863 11.6074 17.2889C10.5241 17.5095 9.58138 17.9611 8.75325 18.274ZM15.6074 18.0175C15.6074 18.0175 15.347 18.3253 13.6647 17.6173C15.4928 17.4839 15.7949 17.8944 15.6074 18.0175Z"
                                    fill="#492396" />
                            </svg>
                            ver brochure
                        </a>
                        <a href="#" class="buttom buttom__rellenoM" data-toggle="modal" data-target="#VistaPanoramicaModal">
                            <svg width="30" height="37" viewBox="0 0 85 52" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M76.6747 18.9948C76.3045 18.7967 75.872 18.7532 75.4703 18.8737C75.0686 18.9942 74.7299 19.2691 74.5271 19.6391C74.3242 20.0091 74.2734 20.4447 74.3857 20.8522C74.4979 21.2596 74.7642 21.6062 75.1272 21.8175C78.7185 23.8208 80.6916 26.1154 80.6916 28.2765C80.6916 33.3772 69.2921 39.4901 50.2137 40.8924C49.7977 40.9177 49.4079 41.1062 49.1277 41.4176C48.8475 41.7289 48.699 42.1385 48.7139 42.5588C48.7288 42.9792 48.906 43.377 49.2076 43.6673C49.5091 43.9576 49.9112 44.1175 50.328 44.1128C50.3671 44.1128 50.4062 44.1128 50.4453 44.1128C70.1373 42.6665 83.8889 36.1574 83.8889 28.281C83.8904 25.8786 82.6392 22.3244 76.6747 18.9948Z"
                                    fill="white" stroke="#492396" stroke-miterlimit="10" />
                                <path
                                    d="M43.469 41.6466L37.0789 35.1983C36.9337 35.0362 36.7574 34.9057 36.5606 34.8145C36.3639 34.7234 36.1508 34.6735 35.9344 34.6681C35.718 34.6626 35.5027 34.7015 35.3017 34.7826C35.1006 34.8637 34.918 34.9851 34.7649 35.1396C34.6119 35.2941 34.4915 35.4784 34.4112 35.6812C34.3308 35.8841 34.2922 36.1013 34.2977 36.3197C34.3031 36.5381 34.3525 36.7531 34.4428 36.9517C34.5331 37.1502 34.6625 37.3282 34.8231 37.4747L38.4159 41.1003C17.9374 40.3718 3.98874 34.0176 3.98874 28.2749C3.98874 26.1138 5.96636 23.8192 9.55315 21.816C9.74883 21.7207 9.92321 21.5862 10.0657 21.4207C10.2081 21.2551 10.3156 21.062 10.3816 20.8531C10.4476 20.6442 10.4707 20.424 10.4496 20.2058C10.4284 19.9876 10.3634 19.776 10.2585 19.584C10.1536 19.3921 10.011 19.2237 9.83945 19.0891C9.6679 18.9546 9.47097 18.8568 9.2607 18.8016C9.05043 18.7465 8.83123 18.7352 8.61647 18.7684C8.40172 18.8016 8.19593 18.8786 8.01166 18.9947C2.04421 22.3244 0.792969 25.8786 0.792969 28.2749C0.792969 36.9708 16.7764 43.6392 38.5407 44.3373L34.8126 48.0995C34.652 48.246 34.5226 48.424 34.4323 48.6225C34.3419 48.8211 34.2926 49.0361 34.2871 49.2545C34.2817 49.4729 34.3203 49.6901 34.4006 49.893C34.481 50.0958 34.6013 50.2801 34.7544 50.4346C34.9075 50.5891 35.0901 50.7105 35.2911 50.7916C35.4922 50.8726 35.7075 50.9116 35.9239 50.9061C36.1403 50.9006 36.3534 50.8508 36.5501 50.7597C36.7469 50.6685 36.9232 50.538 37.0684 50.3759L43.4584 43.9276C43.7579 43.6243 43.9261 43.2137 43.9261 42.7856C43.9261 42.3575 43.7579 41.9468 43.4584 41.6436L43.469 41.6466Z"
                                    fill="white" stroke="#492396" stroke-miterlimit="10" />
                                <path
                                    d="M14.6986 17.3496C18.5771 21.4017 29.0292 30.82 42.3011 30.82C55.5729 30.82 66.0205 21.4017 69.899 17.3496L69.9652 17.2813C70.2921 16.9441 70.4752 16.4912 70.4752 16.0194C70.4752 15.5477 70.2921 15.0947 69.9652 14.7575L69.899 14.6893C66.0205 10.6372 55.5684 1.21741 42.3011 1.21741C29.0337 1.21741 18.5771 10.6372 14.6986 14.6877L14.6324 14.756C14.3059 15.0934 14.123 15.5463 14.123 16.0179C14.123 16.4895 14.3059 16.9424 14.6324 17.2798L14.6986 17.3496ZM42.3011 4.52732C44.5533 4.52732 46.7549 5.20128 48.6276 6.46396C50.5003 7.72665 51.9598 9.52136 52.8217 11.6211C53.6836 13.7209 53.9091 16.0314 53.4697 18.2605C53.0304 20.4896 51.9458 22.5372 50.3532 24.1443C48.7607 25.7514 46.7316 26.8458 44.5227 27.2892C42.3137 27.7326 40.024 27.5051 37.9433 26.6353C35.8625 25.7656 34.084 24.2927 32.8327 22.4029C31.5814 20.5132 30.9136 18.2915 30.9136 16.0187C30.9136 12.971 32.1133 10.0481 34.2489 7.89306C36.3845 5.73801 39.2809 4.52732 42.3011 4.52732V4.52732Z"
                                    stroke="white" stroke-width="2" stroke-miterlimit="10" />
                                <path
                                    d="M42.2999 22.0785C43.5284 22.0789 44.7274 21.6989 45.735 20.9897C46.7426 20.2806 47.5103 19.2766 47.9345 18.1132C48.3587 16.9498 48.419 15.683 48.1071 14.484C47.7952 13.285 47.1262 12.2114 46.1905 11.4082L42.5917 15.316L44.07 10.2275C43.2442 9.97075 42.3733 9.89687 41.5166 10.0109C40.6599 10.1249 39.8377 10.4242 39.1062 10.8882C38.3746 11.3523 37.7509 11.9702 37.2776 12.6997C36.8044 13.4293 36.4927 14.2533 36.3639 15.1155C36.2352 15.9778 36.2923 16.8579 36.5314 17.6958C36.7706 18.5337 37.1861 19.3097 37.7496 19.9707C38.3131 20.6317 39.0114 21.1622 39.7967 21.526C40.582 21.8898 41.4358 22.0782 42.2999 22.0785Z"
                                    fill="white" />
                            </svg>
                            Vista Panorámica
                        </a>
                    </div>

                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="caracteristicas__tabs" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="500">
                    <ul class="nav nav-tabs">
                        <li><a data-toggle="tab" class="active primero" href="#comodidad">comodidad</a></li>
                        <li><a data-toggle="tab" href="#eco">eco-sostenible</a></li>
                        <li><a data-toggle="tab" href="#area">áreas comunes</a></li>
                        <li><a data-toggle="tab" href="#seguridad">seguridad</a></li>
                    </ul>

                    <div class="tab-content caracteristicas__tabs__content">
                        <div id="comodidad" class="tab-pane fade in active show">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/tableros.png" class="img-fluid" alt="">
                                            <h5>Tablero de losa y <br> mueble en baño <br> principal</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/medidor.png" class="img-fluid" alt="">
                                            <h5>Medidor de luz independiente</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/muebles.png" class="img-fluid" alt="">
                                            <h5>Muebles altos y <br> bajos en cocina</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 d-none d-sm-block">

                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/presion.png" class="img-fluid" alt="">
                                            <h5>Sist. de presión constante de <br> agua</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/contometro.png" class="img-fluid" alt="">
                                            <h5>Contómetro de agua para cada depto.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="eco" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/sistema.png" class="img-fluid" alt="">
                                            <h5>Sistema centralizado <br> de agua caliente a <br> gas</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/paneles.png" class="img-fluid" alt="">
                                            <h5>Paneles Solares</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/bicicletas.png" class="img-fluid" alt="">
                                            <h5>48 Estacionamientos para bicicletas</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="area" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/mirador.png" class="img-fluid" alt="">
                                            <h5>Mirador 180º</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/rooftop.png" class="img-fluid" alt="">
                                            <h5>Rooftop</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/sum.png" class="img-fluid" alt="">
                                            <h5>2 SUM</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/coworking.png" class="img-fluid" alt="">
                                            <h5>Coworking</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/lobby.png" class="img-fluid" alt="">
                                            <h5>Lobby de <br> doble altura</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/lavanderia.png" class="img-fluid" alt="">
                                            <h5>Lavandería</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/gimnasio.png" class="img-fluid" alt="">
                                            <h5>Gimnasio</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="seguridad" class="tab-pane fade">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/alarma.png" class="img-fluid" alt="">
                                            <h5>Alarma y cisterna <br> contra incendios</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/biometrico.png" class="img-fluid" alt="">
                                            <h5>Sist. de seguridad biométrico</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/seguridad.png" class="img-fluid" alt="">
                                            <h5>Más de 10 cámaras <br> de seguridad</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 d-none d-sm-block">

                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/circuito.png" class="img-fluid" alt="">
                                            <h5>Circuito cerrado de <br> TV y grabador de <br> video (DVR)</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--morado">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/sotano.png" class="img-fluid" alt="">
                                            <h5>Sistema de <br> extracción CO en <br> sótanos</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-6">
                                    <div class="iconos iconos--verde">
                                        <div class="">
                                            <img src="landing/img/iconos/border.png" class="img-fluid borde" alt="">
                                            <img src="landing/img/iconos/temperatura.png" class="img-fluid" alt="">
                                            <h5>Detectores de humo <br> y temperatura</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal modal__video fade" id="VistaPanoramicaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ url('landing/img/iconos/cerrar.png') }}" alt="">
                </button>
                {!! html_entity_decode($caracteristicas->vistapanoramica) !!}
            </div>
        </div>
    </div>
</div>
