<section class="galeria" id="galeria">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="titulo titulo--grande" data-aos="fade-down">Galería</h2>
            </div>
            <div class="col-lg-12 col-md-12 m-auto">
                <div class="listado filter" data-aos="fade-up">
                    <a href="javascript:void(0)" data-filter="*" class="current">TODOS</a>
                    <a href="javascript:void(0)" data-filter=".exteriores">exteriores</a>
                    <a href="javascript:void(0)" data-filter=".interiores">interiores</a>
                    <a href="javascript:void(0)" data-filter=".area_comunes">Áreas comunes</a>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-grid">
        <div class="item">
            <a href="https://www.youtube.com/watch?v=IfB65qjLbwc" data-fancybox="gallery">
                <img src="landing/img/galeria/1.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
        <div class="item">
            <a href="landing/img/galeria/2.jpg" data-fancybox="gallery">
                <img src="landing/img/galeria/2.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
        <div class="item">
            <a href="landing/img/galeria/3.jpg" data-fancybox="gallery">
                <img src="landing/img/galeria/3.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
        <div class="item">
            <a href="landing/img/galeria/4.jpg" data-fancybox="gallery">
                <img src="landing/img/galeria/4.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
        <div class="item">
            <a href="landing/img/galeria/5.jpg" data-fancybox="gallery">
                <img src="landing/img/galeria/5.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
        <div class="item">
            <a href="landing/img/galeria/6.jpg" data-fancybox="gallery">
                <img src="landing/img/galeria/6.jpg" alt="">
                <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                        alt=""></span>
            </a>
        </div>
    </div>


    <div class="grid" id='add__'>
        <div class="grid-sizer"></div>

        @if ( count($galeria) > 0)

            @foreach ($galeria as $rowimage )

                @if ( $loop->iteration <= 6 )
                <div class="grid-item {{ $rowimage->categoria }}">
                    <a href="{{ url('storage') }}/{{ $rowimage->link_imagen}}" data-fancybox="gallery">
                        <img src="@if( strlen($rowimage->link_imagen_cropped) > 10 ){{ url('storage') }}/{{ $rowimage->link_imagen_cropped}}@else{{ url('storage') }}/{{ $rowimage->link_imagen}} @endif" alt="">
                        <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                                alt=""></span>
                    </a>
                </div>

                @endif
            @endforeach

        @endif

    </div>

    @if( count($galeria) > 6 )
        <div class="cargar-mas text-center" data-aos="fade-up" id='vermas__'>
            <a href="javascript:void(0)" id='page' class="buttom buttom__border mt-4">Cargar más</a>
        </div>
    @endif

</section>


<script type="text/javascript">

    var url = "{{ url('/') }}";
    var total_page = {{ count($galeria) }},
    limit = 4;
</script>
