
<section class="modelo" id="departamentos">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="titulo titulo--grande text-center" data-aos="fade-down">Escoge el modelo ideal</h2>
            </div>
            <div class="col-lg-4 col-md-6 p-0 d-none d-sm-block" data-aos="zoom-in-down">
                <div class="modelo__item modelo__item--raya">
                    <h3 class="text-center">Loft</h3>
                    <div class="modelo__item__content">
                        <img src="landing/img/modelo/1.png" class="img-fluid" alt="">
                    </div>
                    <div class="modelo__item__texto">
                        <span>Desde</span>
                        <p>32-46m2</p>
                        <a href="javascript:void(0)"  categoria='loft'  titulo="Loft"  abrir_modal="true" class="buttom buttom__border">Ver
                            planos</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 p-0 d-none d-sm-block" data-aos="zoom-in-down">
                <div class="modelo__item modelo__item--raya">
                    <h3 class="text-center">Duo</h3>
                    <div class="modelo__item__content">
                        <img src="landing/img/modelo/2.png" class="img-fluid" alt="">
                    </div>
                    <div class="modelo__item__texto">
                        <span>Desde</span>
                        <p>43-56m2</p>
                        <a href="javascript:void(0)" titulo="Duo" categoria='duo' abrir_modal="true" class="buttom buttom__border">Ver
                            planos</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 p-0 d-none d-sm-block" data-aos="zoom-in-down">
                <div class="modelo__item modelo__item--raya">
                    <h3 class="text-center">Social</h3>
                    <div class="modelo__item__content">
                        <img src="landing/img/modelo/3.png" class="img-fluid" alt="">
                    </div>
                    <div class="modelo__item__texto">
                        <span>Desde</span>
                        <p>73-78m2</p>
                        <a href="javascript:void(0)" titulo="Social" categoria='social' abrir_modal="true" class="buttom buttom__border">Ver
                            planos</a>
                    </div>
                </div>
            </div>
            <!--<div class="col-lg-3 col-md-6 p-0 d-none d-sm-block" data-aos="zoom-in-down">
                <div class="modelo__item">
                    <h3 class="text-center">Único</h3>
                    <div class="modelo__item__content">
                        <img src="landing/img/modelo/4.png" class="img-fluid" alt="">
                    </div>
                    <div class="modelo__item__texto">
                        <span>Desde</span>
                        <p>53-56m2</p>
                        <a href="javascript:void(0)" titulo="Único" categoria='unico' abrir_modal="true" class="buttom buttom__border">Ver
                            planos</a>
                    </div>
                </div>
            </div>-->
            <div class="col-12 p-0 d-block d-sm-none" data-aos="zoom-in-down">
                <div class="owl-carousel owl-modelo owl-theme">
                    <div class="item">
                        <div class="modelo__item">
                            <h3 class="text-center">Loft</h3>
                            <div class="modelo__item__content">
                                <img src="landing/img/modelo/1.png" class="img-fluid" alt="">
                            </div>
                            <div class="modelo__item__texto">
                                <span>Desde</span>
                                <p>32-46m2</p>
                                <a href="javascript:void(0)" titulo="Loft" categoria='loft' abrir_modal="true"
                                    class="buttom buttom__borde" >Ver planos</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="modelo__item modelo__item--raya">
                            <h3 class="text-center">Duo</h3>
                            <div class="modelo__item__content">
                                <img src="landing/img/modelo/2.png" class="img-fluid" alt="">
                            </div>
                            <div class="modelo__item__texto">
                                <span>Desde</span>
                                <p>43-56m2</p>
                                <a href="javascript:void(0)" titulo="Duo" categoria='duo' abrir_modal="true"
                                    class="buttom buttom__borde">Ver planos</a>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="modelo__item">
                            <h3 class="text-center">Social</h3>
                            <div class="modelo__item__content">
                                <img src="landing/img/modelo/3.png" class="img-fluid" alt="">
                            </div>
                            <div class="modelo__item__texto">
                                <span>Desde</span>
                                <p>73-78m2</p>
                                <a href="javascript:void(0)" titulo="Social" categoria='social' abrir_modal="true"
                                    class="buttom buttom__borde">Ver planos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
