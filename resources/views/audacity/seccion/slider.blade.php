<section class="slider1" id="inicio">
    <div id="carouselExampleIndicators" class="carousel slide home__slider" data-ride="carousel">
        <a href="#">
            <div id="mouse-scroll">
                <div class="mouse">
                    <div class="mouse-in"></div>
                </div>
                <span>Scroll</span>
                <div class="raya">
                </div>
            </div>
        </a>

        <ol class="carousel-indicators carousel-indicators--principal">
            @foreach ( $slider as $row_slider )
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration - 1 }}" class="@if ($loop->first) active @endif"><span>{{ str_pad($loop->iteration, 2, "0", STR_PAD_LEFT) }}</span> </li>
            @endforeach
        </ol>


        <div class="carousel-inner">

            @foreach ( $slider as $row_slider )

            <div class="carousel-item  @if ($loop->first) active @endif">
                <div class="img zoom" style="background-image:url('landing/img/slider/{{ $loop->iteration }}.jpg');">
                </div>
                <div class="fondoTexto animate__animated animate__fadeInUpBig animate__delay-3s">
                    <h2>AUDACITY</h2>
                </div>
                <div class="carousel-texto d-flex align-items-center">
                    <div class="">
                        <img class="logo animate__animated animate__backInDown" src="landing/img/logotipo.png"
                            alt="">
                        <h2 class="animate__animated animate__zoomInDown animate__delay-1s">{{ $row_slider->title }}
                            <span>{{ $row_slider->title_personalizado }}</span> </h2>
                            @if ( $row_slider->tipo_subtitulo == 'direccion' ) <div class="direccion animate__animated animate__zoomInUp animate__delay-2s">@endif
                            @if ( $row_slider->tipo_subtitulo == 'direccion' ) <img src="landing/img/iconos/location.png" alt=""> @endif
                            <p class="@if( $row_slider->tipo_subtitulo == 'descripcion')p-2 animate__animated animate__zoomInUp animate__delay-2s @endif">{{ $row_slider->subtitulo }}</p>
                            @if ( $row_slider->tipo_subtitulo == 'direccion' )</div>@endif
                    </div>
                </div>
                <div class="carousel-img @if ($loop->first) carousel-img--centrado @endif  d-flex justify-content-end">
                    <div class="">
                        <img src="{{ url('storage') }}/{{ $row_slider->ruta_imagen }}"
                            class="img-fluid animate__animated animate__fadeInDown" alt="">
                    </div>
                </div>
            </div>

            @endforeach

<!--
            <div class="carousel-item">
                <div class="img zoom" style="background-image:url('landing/img/slider/2.jpg');">
                </div>
                <div class="fondoTexto animate__animated animate__fadeInUpBig animate__delay-3s">
                    <h2 class="">AUDACITY</h2>
                </div>
                <div class="carousel-texto d-flex align-items-center">
                    <div class="">
                        <img class="logo animate__animated animate__backInDown" src="landing/img/logotipo.png"
                            alt="">
                        <h2 class="animate__animated animate__zoomInDown animate__delay-1s">te eleva a otra
                            <span>altura</span> </h2>
                        <p class="p-2 animate__animated animate__zoomInUp animate__delay-2s">Un desarrollo que te
                            eleva a otra altura, la verticalización del futuro.</p>
                    </div>
                </div>
                <div class="carousel-img d-flex justify-content-end">
                    <div class="">
                        <img src="landing/img/slider/slider2.png"
                            class="img-fluid animate__animated animate__fadeInDown" alt="">
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="img zoom" style="background-image:url('landing/img/slider/3.jpg');">
                </div>
                <div class="fondoTexto animate__animated animate__fadeInUpBig animate__delay-2s">
                    <h2>AUDACITY</h2>
                </div>
                <div class="carousel-texto d-flex align-items-center">
                    <div class="">
                        <img class="logo animate__animated animate__backInDown" src="landing/img/logotipo.png"
                            alt="">
                        <h2 class="animate__animated animate__zoomInDown animate__delay-1s">Desarrollo EN TODAS LAS
                            <span> NUEVAS ALTURAS </span> </h2>
                    </div>
                </div>
                <div class="carousel-img d-flex justify-content-end">
                    <div class="">
                        <img src="landing/img/slider/slider3.png"
                            class="img-fluid animate__animated animate__fadeInDown" alt="">
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>
