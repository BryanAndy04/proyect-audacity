<section class="descripcion" id="descripcion">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-12 offset-lg-1">
                <span class="titulo titulo--chico" data-aos="fade-right" data-aos-easing="ease" data-aos-delay="600">La
                    ciudad es de quien se atreve a vivirla.</span>
                <h2 class="titulo titulo--grande" data-aos="fade-right" data-aos-easing="ease" data-aos-delay="900">
                    Un desarrollo que te eleva a otra altura, la verticalización del futuro.</h2>
                <p class="parrafo" data-aos="fade-right" data-aos-easing="ease" data-aos-delay="1300">Javier Padro
                    Este N° 1501.</p>
                <div class="botones" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="1600">
                    <a href="#" data-toggle="modal" data-target="#ModalDescripcion" class="buttom buttom__rellenoV"><svg
                            width="38" height="30" viewBox="0 0 38 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M23.9188 25.835C23.3665 25.835 22.8872 25.4245 22.8156 24.8623C22.7382 24.2524 23.1696 23.6951 23.7793 23.6174C27.2972 23.1692 30.4474 22.216 32.6496 20.9328C34.6637 19.7595 35.7726 18.3801 35.7726 17.0488C35.7726 15.5815 34.4749 14.381 33.386 13.6318C32.8795 13.2833 32.7514 12.5904 33.0999 12.0837C33.4483 11.5772 34.1415 11.449 34.648 11.7975C36.8404 13.3057 37.9992 15.1214 37.9992 17.0491C37.9992 19.2368 36.5371 21.245 33.7707 22.8567C31.2806 24.3074 27.9231 25.3343 24.0608 25.8263C24.013 25.8321 23.9654 25.835 23.9188 25.835Z" />
                            <path
                                d="M18.7287 24.2401L15.7599 21.2714C15.325 20.8365 14.6202 20.8365 14.1854 21.2714C13.7508 21.7059 13.7508 22.411 14.1854 22.8456L15.0528 23.713C11.7269 23.3695 8.70535 22.5951 6.36572 21.47C3.7353 20.205 2.22657 18.5934 2.22657 17.0487C2.22657 15.7386 3.3068 14.3768 5.26809 13.2143C5.79719 12.9009 5.97172 12.2178 5.65832 11.689C5.34463 11.1599 4.66158 10.9854 4.13277 11.2988C0.716966 13.3236 0 15.5432 0 17.0487C0 19.5188 1.91809 21.8016 5.40087 23.4768C8.10406 24.7765 11.6008 25.6471 15.4103 25.984L14.1854 27.2089C13.7508 27.6435 13.7508 28.3485 14.1854 28.7834C14.4028 29.0006 14.6878 29.1093 14.9728 29.1093C15.2575 29.1093 15.5425 29.0006 15.7599 28.7834L18.7287 25.8147C19.1633 25.3798 19.1633 24.6747 18.7287 24.2401Z"
                                fill="#492396" />
                            <path
                                d="M11.6598 14.8193V14.5514C11.6598 13.6066 11.0814 13.4233 10.3058 13.4233C9.82631 13.4233 9.67121 13.0004 9.67121 12.5774C9.67121 12.1541 9.82631 11.7311 10.3058 11.7311C10.8416 11.7311 11.4058 11.6607 11.4058 10.5184C11.4058 9.70052 10.9405 9.50309 10.3621 9.50309C9.67121 9.50309 9.31867 9.6724 9.31867 10.2224C9.31867 10.7016 9.10703 11.026 8.28917 11.026C7.27388 11.026 7.14719 10.8144 7.14719 10.1374C7.14719 9.03777 7.93663 7.6134 10.3621 7.6134C12.1532 7.6134 13.5068 8.26195 13.5068 10.1658C13.5068 11.195 13.1262 12.1541 12.4211 12.4785C13.2531 12.7887 13.8594 13.4091 13.8594 14.5514V14.8193C13.8594 17.132 12.266 18.0064 10.2916 18.0064C7.86618 18.0064 6.93555 16.5258 6.93555 15.3412C6.93555 14.7065 7.20343 14.5372 7.97896 14.5372C8.88147 14.5372 9.10703 14.7346 9.10703 15.2707C9.10703 15.9335 9.72774 16.0886 10.3621 16.0886C11.3211 16.0886 11.6598 15.736 11.6598 14.8193Z"
                                fill="#492396" />
                            <path
                                d="M22.4182 14.5514V14.6784C22.4182 17.1039 20.9092 18.0064 18.9633 18.0064C17.0174 18.0064 15.4941 17.1039 15.4941 14.6784V10.9414C15.4941 8.51592 17.0594 7.6134 19.0903 7.6134C21.4734 7.6134 22.4182 9.09401 22.4182 10.2644C22.4182 10.9414 22.0938 11.1527 21.3887 11.1527C20.7825 11.1527 20.2465 10.9976 20.2465 10.3491C20.2465 9.8133 19.6826 9.53121 19.0198 9.53121C18.1878 9.53121 17.694 9.9684 17.694 10.9414V12.2103C18.1454 11.7169 18.7801 11.5899 19.457 11.5899C21.0643 11.5899 22.4182 12.295 22.4182 14.5514ZM17.694 14.8335C17.694 15.8065 18.1736 16.2295 18.9633 16.2295C19.753 16.2295 20.2183 15.8065 20.2183 14.8335V14.7065C20.2183 13.677 19.753 13.2822 18.9491 13.2822C18.1878 13.2822 17.694 13.6489 17.694 14.5795V14.8335Z"
                                fill="#492396" />
                            <path
                                d="M24.125 14.6784V10.9414C24.125 8.51592 25.6337 7.6134 27.5799 7.6134C29.5259 7.6134 31.0488 8.51592 31.0488 10.9414V14.6784C31.0488 17.1039 29.5259 18.0064 27.5799 18.0064C25.6337 18.0064 24.125 17.1039 24.125 14.6784ZM28.8489 10.9414C28.8489 9.9684 28.3697 9.53121 27.5799 9.53121C26.7902 9.53121 26.3249 9.9684 26.3249 10.9414V14.6784C26.3249 15.6514 26.7902 16.0886 27.5799 16.0886C28.3697 16.0886 28.8489 15.6514 28.8489 14.6784V10.9414Z"
                                fill="#492396" />
                            <path
                                d="M33.7207 7.60353C31.8789 7.60353 30.3809 6.10523 30.3809 4.26368C30.3809 2.42212 31.8789 0.923828 33.7207 0.923828C35.5623 0.923828 37.0606 2.42212 37.0606 4.26368C37.0606 6.10523 35.5623 7.60353 33.7207 7.60353ZM33.7207 3.15039C33.1067 3.15039 32.6074 3.64992 32.6074 4.26368C32.6074 4.87772 33.1067 5.37696 33.7207 5.37696C34.3345 5.37696 34.834 4.87772 34.834 4.26368C34.834 3.64992 34.3345 3.15039 33.7207 3.15039Z"
                                fill="#492396" />
                        </svg> Experiencia 360</a> <br><br>

                    <a href="#" data-toggle="modal" data-target="#exampleModal" class="buttom buttom__rellenoM">
                        <svg width="23" height="25" viewBox="0 0 23 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M19.8086 11.2169C20.8086 11.7943 20.8086 13.2376 19.8086 13.815L4.05955 22.9077C3.05955 23.4851 1.80956 22.7634 1.80956 21.6087V3.42324C1.80956 2.26855 3.05955 1.54686 4.05956 2.12421L19.8086 11.2169Z"
                                stroke-width="3" />
                        </svg>
                        Ver vídeo</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- descripcion 360 -->

<div class="modal modal__descrip fade" id="ModalDescripcion" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ url('landing/img/iconos/cerrar.png') }}" alt="">
                </button>
                <div class="container-fluid p-0">
                    <div class="row">
                        <div class="col-12">
                            <div class="modal__descrip__tabs">
                                <ul class="nav nav-tabs">

                                    @if ( strlen( $descripcion->exp_loft_url ) > 10 )
                                    <li><a data-toggle="tab" class="active primero" href="#loft">Loft</a></li>
                                    @endif

                                    @if ( strlen( $descripcion->exp_duo_url ) > 10 )
                                    <li><a data-toggle="tab" href="#duo">Duo</a></li>
                                    @endif

                                    @if ( strlen( $descripcion->exp_social_url ) > 10 )
                                    <li><a data-toggle="tab" href="#social">Social</a></li>
                                    @endif

                                    @if ( strlen( $descripcion->exp_area_com_url ) > 10 )
                                    <li><a data-toggle="tab" href="#areaC">Áreas comunes</a></li>
                                    @endif

                                </ul>
                                <div class="tab-content modal__descrip__tabs__content">
                                    @if ( strlen( $descripcion->exp_loft_url ) > 10 )
                                    <div id="loft" class="tab-pane fade in active show">
                                        <iframe
                                            src='https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/'
                                            frameborder='0' allowfullscreen='allowfullscreen'></iframe>
                                    </div>
                                    @endif

                                    @if ( strlen( $descripcion->exp_duo_url ) > 10 )
                                    <div id="duo" class="tab-pane fade">
                                        <iframe
                                            src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                                    </div>
                                    @endif

                                    @if ( strlen( $descripcion->exp_social_url ) > 10 )
                                    <div id="social" class="tab-pane fade">
                                        <iframe
                                            src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                                    </div>
                                    @endif

                                    @if ( strlen( $descripcion->exp_area_com_url ) > 10 )
                                    <div id="areaC" class="tab-pane fade">
                                        <iframe
                                            src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                                    </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal__video fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{ url('landing/img/iconos/cerrar.png') }}" alt="">
                </button>
                {!! html_entity_decode($descripcion->link_video) !!}
            </div>
        </div>
    </div>
</div>
