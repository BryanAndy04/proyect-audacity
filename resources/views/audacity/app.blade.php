<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title> Proyecto Audacity Departamentos en Javier Prado Este | Senda </title>
    @include('audacity.include.header')
</head>

<body>
    @include('audacity.include.menu')
    @include('audacity.include.asesorate', [ 'tipos_departamentos' => $tipos_departamentos ])

    <a href="javascript:void(0)" class="iconAsesorate asesorateI">
        <div class="iconAsesorate__interno">
            <div class="iconAsesorate__interno__verde">
                <svg width="36" height="28" viewBox="0 0 36 28" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M20.9514 18.7835C23.0673 17.8916 24.7372 16.6775 25.9609 15.1408C27.1848 13.6045 27.7968 11.9314 27.7968 10.1214C27.7968 8.31145 27.1848 6.63842 25.9609 5.1018C24.7372 3.56538 23.0673 2.35117 20.9514 1.45904C18.8355 0.567043 16.5341 0.121216 14.0469 0.121216C11.5599 0.121216 9.25857 0.567317 7.14252 1.45931C5.02673 2.35131 3.35678 3.56552 2.133 5.102C0.908938 6.63842 0.296875 8.31138 0.296875 10.1214C0.296875 11.6708 0.759599 13.1298 1.68395 14.4967C2.60831 15.8639 3.87793 17.0165 5.4924 17.9538C5.36228 18.2666 5.22882 18.5527 5.09207 18.8135C4.95525 19.0738 4.79264 19.3243 4.60369 19.5654C4.41488 19.8065 4.26855 19.9952 4.16429 20.1314C4.0601 20.2681 3.89086 20.4604 3.65642 20.7076C3.42191 20.955 3.27209 21.1178 3.20717 21.1957C3.20717 21.1826 3.18097 21.2121 3.12898 21.2837C3.07678 21.3554 3.0475 21.3877 3.04093 21.3816C3.0345 21.3746 3.00837 21.4072 2.96288 21.4788C2.91732 21.5505 2.89447 21.5863 2.89447 21.5863L2.84555 21.6836C2.82613 21.7225 2.81299 21.7614 2.80656 21.8007C2.79999 21.8395 2.79671 21.8821 2.79671 21.9275C2.79671 21.973 2.80314 22.0151 2.81627 22.0544C2.84248 22.2234 2.91732 22.3599 3.04093 22.4646C3.16469 22.5686 3.29795 22.6205 3.4414 22.6205H3.50003C4.15095 22.5294 4.71095 22.4252 5.17963 22.3079C7.18486 21.787 8.99492 20.9536 10.6095 19.8079C11.7812 20.0162 12.9272 20.1204 14.0469 20.1204C16.5339 20.1212 18.8355 19.6755 20.9514 18.7835ZM10.0237 17.1529L9.16416 17.7581C8.79954 18.0053 8.39613 18.2596 7.95331 18.5202L8.63707 16.8793L6.74252 15.7856C5.49226 15.0566 4.52235 14.1972 3.83223 13.2075C3.14204 12.218 2.79712 11.1893 2.79712 10.1215C2.79712 8.79319 3.30841 7.54971 4.33046 6.39084C5.35237 5.23198 6.72939 4.31406 8.46118 3.63673C10.1928 2.95982 12.0549 2.62105 14.0471 2.62105C16.0392 2.62105 17.9014 2.95982 19.633 3.63673C21.3647 4.31399 22.7417 5.23198 23.7641 6.39084C24.7862 7.54964 25.2971 8.79313 25.2971 10.1215C25.2971 11.4496 24.7862 12.693 23.7641 13.8519C22.7417 15.0111 21.3647 15.9288 19.633 16.6058C17.9014 17.2829 16.0394 17.6214 14.0471 17.6214C13.0707 17.6214 12.0746 17.5301 11.0588 17.348L10.0237 17.1529Z"
                        fill="#492396" />
                    <path
                        d="M33.9101 19.5055C34.8349 18.1444 35.2969 16.6833 35.2969 15.1206C35.2969 13.519 34.8086 12.0214 33.8324 10.6284C32.8557 9.23542 31.5274 8.07635 29.8478 7.15186C30.1471 8.12834 30.2968 9.11789 30.2968 10.1207C30.2968 11.8654 29.8612 13.519 28.9882 15.0817C28.1158 16.6439 26.8658 18.024 25.2382 19.2222C23.7277 20.3159 22.0089 21.1555 20.0817 21.7416C18.155 22.3274 16.1431 22.6206 14.0467 22.6206C13.6562 22.6206 13.0832 22.5948 12.3281 22.5428C14.9452 24.2612 18.0183 25.1208 21.5468 25.1208C22.6667 25.1208 23.8124 25.0164 24.9844 24.8081C26.5989 25.9543 28.409 26.7873 30.4141 27.3084C30.8829 27.4259 31.4428 27.5299 32.0939 27.6209C32.25 27.6343 32.3936 27.5884 32.5237 27.4844C32.6539 27.38 32.7386 27.2372 32.7775 27.0551C32.7714 26.9769 32.7775 26.934 32.7971 26.9278C32.8163 26.9216 32.8131 26.8791 32.7873 26.8011C32.7615 26.7228 32.7484 26.6838 32.7484 26.6838L32.6997 26.5863C32.6862 26.5606 32.6641 26.5246 32.6314 26.4792C32.5988 26.434 32.5729 26.4012 32.5532 26.3815C32.534 26.3621 32.5049 26.3293 32.4656 26.2841C32.4267 26.239 32.4005 26.2094 32.3874 26.1963C32.3224 26.1182 32.1727 25.9555 31.9384 25.708C31.7038 25.4608 31.5348 25.2688 31.4306 25.1321C31.3264 24.9953 31.1799 24.8067 30.9912 24.5655C30.8025 24.3249 30.6395 24.0741 30.5028 23.8136C30.3661 23.5533 30.2326 23.2667 30.1024 22.9544C31.7165 22.0161 32.9863 20.8671 33.9101 19.5055Z"
                        fill="#492396" />
                </svg>
                <span>Asesórate</span>
            </div>
        </div>
    </a>

    @include('audacity.seccion.slider',[ 'slider' => $slider ] )
    @include('audacity.seccion.descripcion',[ 'descripcion' => $descripcion ] )
    @include('audacity.seccion.caracteristicas',[ 'caracteristicas' => $caracteristicas ])
    @include('audacity.seccion.galeria', [ 'galeria' => $galeria ])
    @include('audacity.seccion.departamentos')


    <section class="mapa" id="ubicacion">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12">
                    <img src="landing/img/bg/map.png" class="img-fluid imagen" alt="" data-aos="flip-left"
                        data-aos-duration="1000">
                </div>
                <div class="col-xl-3 col-lg-4 col-md-12 col-12 d-flex align-items-center">
                    <div class="mapa__content" data-aos="fade-right">
                        <h2 class="titulo titulo--grande">UBICACIÓN <br> DEL PROYECTO</h2>
                        <div class="icono">
                            <img src="landing/img/iconos/location.png" class="img-fluid" alt="">
                            <p>Av. Javier Prado Este Nº1501</p>
                        </div>
                        <div class="texto">
                            <p class="parrafo">Un desarrollo que te eleva a otra altura, la verticalización del futuro.
                                AUDACITY se establece en la Av. Javier Prado Este N° 1501, un lugar de oportunidades y
                                movimiento. El lugar perfecto para revolucionar la vida urbana. La ciudad es de quien se
                                atreve a vivirla.</p>
                        </div>
                        <div class="mb-3">
                            <a href="#" data-toggle="modal" data-target="#modalMapa"
                                class="buttom buttom__rellenoV ">ver en google maps</a>
                        </div>
                        <div class="d-block d-sm-none">
                            <a href="https://waze.com/ul?ll=-12.0887486,-77.0131809&z=10"
                                class="buttom buttom__rellenoV"><i class="fab fa-waze"></i> ver en waze</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contruyendo d-none" id="estado">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-4 offset-lg-1">
                    <h2 class="titulo titulo--grande" data-aos="fade-right">Sigue la construcción</h2>
                </div>
                <div class="col-xl-5 col-lg-7">
                    <div class="row seleccionado">
                        <div class="col-lg-3 text-right d-flex align-items-center">
                            <span>seleccionar periodo:</span>
                        </div>
                        <div class="col-lg-5">
                            <div class="dropdown">
                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    14 Enero, 2020
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Lima</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <!--<div class="porcentaje">
                <div class="c-10">
                  <h3>Avance total de la obra</h3>
                </div>
              </div>-->
                            <div class="porcentaje">
                                <div class="my-progress-bar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="owl-carousel owl-grid">
            <div class="item">
                <a href="https://www.youtube.com/watch?v=IfB65qjLbwc" data-fancybox="gallery">
                    <img src="landing/img/construccion/1.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="item">
                <a href="landing/img/construccion/2.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/2.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="item">
                <a href="landing/img/construccion/3.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/3.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="item">
                <a href="landing/img/construccion/4.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/4.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
        </div>
        <div class="grid">
            <div class="grid-sizer"></div>
            <div class="grid-item" rel='peru'>
                <a href="https://www.youtube.com/watch?v=IfB65qjLbwc" data-fancybox="gallery">
                    <img src="landing/img/construccion/1.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="grid-item" rel='peru'>
                <a href="landing/img/construccion/2.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/2.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="grid-item" rel='peru'>
                <a href="landing/img/construccion/3.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/3.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
            <div class="grid-item" rel='peru'>
                <a href="landing/img/construccion/4.jpg" data-fancybox="gallery">
                    <img src="landing/img/construccion/4.jpg" alt="">
                    <span class="grid__magnifier"><img src="landing/img/iconos/zoom.png" class="grid__tp-info"
                            alt=""></span>
                </a>
            </div>
        </div>
        <div class="cargar-mas text-center">
            <a href="#" class="buttom buttom__border mt-4">Cargar más</a>
        </div>
    </section>


    <div class="video">
        <a href="#" data-toggle="modal" data-target="#exampleModal" class="play">
            <div class="icono">
                <img src="landing/img/iconos/play.png" alt="">
            </div>
        </a>
    </div>


    <!-- modal Texto -->
    <div class="modal modal__texto fade" id="modalTexto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <img src="{{ url('landing/img/iconos/cerrar.png') }}" alt="">
                    </button>
                    <h2 class="titulo titulo--grande">Transparencia</h2>
                    <p class="parrafo">{!! html_entity_decode($transparencia->descripcion) !!}</p>
                </div>
            </div>
        </div>
    </div>

    @include('audacity.include.footer',['contactos' => $contactos])
    @include('audacity.include.modal')
    @include('audacity.include.script')
</body>

</html>
