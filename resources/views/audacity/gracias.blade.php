<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  @include('audacity.include.header')
</head>
<body>
  <section class="gracias d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 m-auto">
          <div class="gracias__contenido">
            <div class="marcoAgua d-flex align-items-center">
              <h2>GRACIAS</h2>
            </div>
            <img src="img/iconos/enviarIcon.png" alt="">
            <h1>Cotización enviada con éxito</h1>
            <p>Nuestros asesores se pondran en contacto con usted</p>
            <a href="index.php" class="buttom buttom__rellenoV">Volver al inicio</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  @include('audacity.include.script')
</body>
</html>

