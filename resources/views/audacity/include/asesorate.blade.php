<div class="asesorateM" id="asesorateF">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-lg-6 col-12 p-0 d-none d-sm-block">
                <div class="asesorateM__bg ">
                    <div class="">
                        <img src="{{ url('landing/img/iconos/asesorate-icon.png') }}" alt="" class="img-fluid">
                        <h1>Asesórate</h1>
                        <p>Déjanos tus datos y el tipo de departamento que estas interesado y nos pondremos en contacto
                            con usted.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-12 offset-lg-1 p-0 d-flex align-items-center">
                <div class="asesorateM__formulario">
                    <h2 class="titulo titulo--grande d-block d-sm-none">Asesorate</h2>
                    <a href="javascript:void(0)" class="cerrarA cerrar"><img
                            src="{{ url('landing/img/iconos/cerrar.png') }}" alt=""> </a>
                    <form action="{{ url('asesorate/confirmacion') }}" method="post">
                        @csrf
                        <input type="hidden" name="tipodocumento" id="tipodocumento_selected" value='1'>
                        <input type="hidden" name="tipodepartamento" id="tipodepartamento_selected" value="@foreach($tipos_departamentos as $tipodepartamento)@if( $loop->first ){{ $tipodepartamento->tipo }}@endif @endforeach">

                        <div class="form-group">
                            <label for="exampleInputPassword1">Tipo de departamento</label>

                            <div class="seleccionar">

                                @foreach ( $tipos_departamentos as $tipodepartamento)
                                @if ( $loop->first )
                                <div id="selector" class="seleccionar__top d-flex align-items-center">
                                    <div class="icono">
                                        <img src="{{ url('storage') }}/{{ $tipodepartamento->img_principal }}"
                                            class="img-fluid" alt="" id='img_tipo_departamento'>
                                    </div>
                                    <span id='tipodepartamentodes__'>{{ $tipodepartamento->tipo }}</span>
                                </div>
                                @endif
                                @endforeach

                                <div class="seleccionar__bottom" id="div_contend_tipo_depatamento">

                                    @foreach ( $tipos_departamentos as $tipodepartamento)
                                    <a class="seleccionar__bottom__card" onclick="tipo_depatamento__(this)">
                                        <div class="icono">
                                            <img src="{{ url('storage') }}/{{ $tipodepartamento->img_principal }}"
                                                class="img-fluid" alt="">
                                        </div>
                                        <h3>{{ $tipodepartamento->tipo }}</h3>
                                    </a>
                                    @endforeach

                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Nombres y apellidos</label>
                            <input type="text" class="form-control" required name="nombreapellido">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Tipo de Docuemnto</label>
                            <div class="dropdown">
                                <button class="dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    DNI
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" valor="1" onclick="tipodocumento__(this)">DNI</a>
                                    <a class="dropdown-item" valor="2" onclick="tipodocumento__(this)">RUC</a>
                                    <a class="dropdown-item" valor="3" onclick="tipodocumento__(this)">CI (cedula de identidad)</a>
                                    <a class="dropdown-item" valor="4" onclick="tipodocumento__(this)">Carnet de Extranjería</a>
                                    <a class="dropdown-item" valor="5" onclick="tipodocumento__(this)">Pasaporte </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Nº de Documento</label>
                            <input type="text" class="form-control" required name='numerodocumento'>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Correo</label>
                            <input type="email" class="form-control" required name="email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Teléfono</label>
                            <input type="text" class="form-control" required name="numerotelefonico">
                        </div>

                        <div class="form-group">
                            <div class="check">
                                <input type="checkbox" name="category" id="c_1" class="css-checkbox" required />
                                <label for="c_1" class="css-label">He leído la política de privacidad web.</label>
                            </div>
                            <div class="check">
                                <input type="checkbox" name="publi_promociones" id="c_2" class="css-checkbox" />
                                <label for="c_2" class="css-label">Autorizo a que me envíen publicidad y/o
                                    promociones.</label>
                            </div>
                            <div class="check">
                                <input type="checkbox" name="info_promociones" id="c_3" class="css-checkbox" />
                                <label for="c_3" class="css-label">Autorizo que mi información sea compartida con el
                                    resto de empresas del grupo.</label>
                            </div>
                        </div>

                        <!-- <a href="gracias.php"  class="buttom buttom__rellenoV">Enviar cotización</a>-->
                        <button type="submit" class="buttom buttom__rellenoV">Enviar cotización</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
