<meta name="viewport" content="width=device-width, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" href="{{ url('css/app.css?v=3.5') }}">
<meta name="theme-color" content="#492396">
<link rel="shortcut icon" href="{{ url('landing/img/favicon.ico') }}">
