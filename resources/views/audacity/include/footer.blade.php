<footer class="footer">
    <div class="footer__top">
      <div class="container d-none d-sm-block">
        <div class="row footer__top__uno">
          <div class="col-lg-3 col-12 d-flex align-items-center justify-content-center">
            <div class="footer__top__botom">
              <a href="#"  data-toggle="modal" data-target="#modalTexto" class="buttom buttom__rellenoV">TRANSPARENCIA SENDA</a>
            </div>
          </div>
          <div class="col-lg-6 col-12 d-flex align-items-center justify-content-center">
            <div class="footer__top__links">
              <a href="https://senda.pe/codigo-de-proteccion-al-consumidor/" target="_blank">POLÍTICA DE PRIVACIDAD</a>
              <a href="https://senda.pe/libro-de-reclamaciones/" target="_blank">LIBRO DE RECLAMACIONES</a>
              <a href="https://senda.pe/politica-anticorrupcion/" target="_blank">POLÍTICA ANTICORRUPCIÓN</a>
            </div>
          </div>
          <div class="col-lg-3 col-12">
            <div class="footer__top__redes">
              <ul class="social-icons">
                <li><a href="https://www.facebook.com/SendaInmobiliaria/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UC3HJIEG1a0Pm7gGxfOBpMrQ/featured" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <li><a href="https://www.linkedin.com/company/inmobiliaria-senda/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.instagram.com/sendainmobiliaria_/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-waze"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="container-fluid p-right">
        <div class="row footer__top__dos">
          <div class="col-lg-5 col-md-6 col-12">
            <div class="row">

              @foreach ($contactos as $__)

                <div class="col-lg-6 p-0">
                    <div class="footer__top__dos__card d-flex align-items-center">
                    <div class="">
                        <img src="{{ url($__['icom__']) }}" alt="">
                        <span>{{ $__['titulo'] }}</span>
                        <h3>{{ $__['nombre'] }}</h3>
                        <a href="https://api.whatsapp.com/send?phone={{ $__['num_wa'] }}" target="_blank"><i class="fab fa-whatsapp"></i></a>
                    </div>
                    </div>
                </div>

              @endforeach

            </div>
          </div>
          <div class="col-lg-7 col-md-6 col-12">
            <div class="row">
              <div class="col-lg-3 col-12 col-md-6">
                <div class="footer__top__dos__img">
                  <img src="{{ url('landing/img/logos/place.png') }}" class="img-fluid" alt="">
                </div>
              </div>
              <div class="col-lg-3 col-12 col-md-6">
                <div class="footer__top__dos__img">

                  <img src="{{ url('landing/img/logos/mi-vivienda.png') }}" class="img-fluid" alt="">
                </div>
              </div>
              <div class="col-lg-3 col-12 col-md-6">
                <div class="footer__top__dos__img">
                  <div class="">
                    <span>Pertenecemos al grupo </span>
                    <img src="{{ url('landing/img/logos/echeverria.png') }}" class="img-fluid" alt="">
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-12 col-md-6">
                <div class="footer__top__dos__img">
                  <img src="{{ url('landing/img/logotipo.png') }}" class="img-fluid senda" alt="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container d-block d-sm-none">
        <div class="row footer__top__uno">
          <div class="col-lg-3 col-12">
            <div class="footer__top__botom">
              <a href="#"  data-toggle="modal" data-target="#modalTexto" class="buttom buttom__rellenoV">TRANSPARENCIA SENDA</a>
            </div>
          </div>
          <div class="col-lg-6 col-12 d-flex align-items-center justify-content-center">
            <div class="footer__top__links">
              <a href="https://senda.pe/codigo-de-proteccion-al-consumidor/" target="_blank">POLÍTICA DE PRIVACIDAD</a>
              <a href="https://senda.pe/libro-de-reclamaciones/" target="_blank">LIBRO DE RECLAMACIONES</a>
              <a href="https://senda.pe/politica-anticorrupcion/" target="_blank">POLÍTICA ANTICORRUPCIÓN</a>
            </div>
          </div>
          <div class="col-lg-3 col-12">
            <div class="footer__top__redes">
              <ul class="social-icons">
                <li><a href="https://www.facebook.com/SendaInmobiliaria/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://www.youtube.com/channel/UC3HJIEG1a0Pm7gGxfOBpMrQ/featured" target="_blank"><i class="fab fa-youtube"></i></a></li>
                <li><a href="https://www.linkedin.com/company/inmobiliaria-senda/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="https://www.instagram.com/sendainmobiliaria_/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-waze"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer__bottom">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h6>© Copyright Senda inmobiliaria, Audacity 2020</h6>
          </div>
          <div class="col-lg-6">
            <div class="agencia">
              <p>Diseñada e implementada por: <a href="https://webtilia.com/" target="_blank">Agencia de Marketing Digital Webtilia</a> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>

