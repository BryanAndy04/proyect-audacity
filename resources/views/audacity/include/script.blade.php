<script src="{{ url('js/app.js') }}"></script>
<script type="text/javascript">

    $(function () {

        /*
        $('a[valor_tipo_departamento]').click(function() {
            var valor = $(this).attr('valor_tipo_departamento');
            $('#dropdowntipo').text($(this).text());
            data_tipo(valor);
        })
*/

        $('a[abrir_modal="true"]').click(function () {

            var categoria = $(this).attr('categoria');
            var id_tipo, select;
            $('#titulo___').text($(this).attr('titulo'));
            var list_tipos = "", owl_carousel__ = "";




            $.get(url + "/departamentos/data/tipo", { categoria: categoria, _token: $('meta[name="csrf-token"]').attr('content') }, function (response) {


                //owl_carousel__ += '<div class="owl-carousel owl-modelo__ owl-theme">';
                $.each(response.data, function (indexInArray, value) {

                    if (indexInArray === 0) {
                        id_tipo = value.id;
                        select = value.tipo;
                    }

                    list_tipos += '<a class="dropdown-item" valor_tipo_departamento="' + value.id + '" onclick="change_tipo(this)">' + value.tipo + '</a>';


                    /*
                    owl_carousel__ += '<div class="item">';
                    owl_carousel__ += '<img src="' + url + '/storage/' + value.img_principal + '" class="img-fluid" alt="">';
                    owl_carousel__ += '</div>';*/

                });

               // owl_carousel__ += "</div>";

/*
                $('#carrusel_').html(owl_carousel__);

                var owl = $('.owl-modelo__');
                owl.owlCarousel('destroy');
                owl.on('refresh.owl.carousel refreshed.owl.carousel');
                owl.owlCarousel({
                    loop: true,
                    nav: true,
                    lazyLoad: true,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 1
                        },
                        1000: {
                            items: 1
                        }
                    }
                });
*/

                $('#option_cesta_tipo').html(list_tipos);
                $('#dropdowntipo').text(select);
                data_tipo(id_tipo);

            });

            $('#ModalModelo').modal('show')
        })



    })


    var tipodocumento = 1;
    var tipodepartamento_ = $('#tipodepartamentodes__').text();

    function tipodocumento__(id) {
        $('#dropdownMenuButton').text($(id).text());
        var tipodocumento = $(id).attr('valor');
        $('#tipodocumento_selected').val(tipodocumento);
    }

    function tipo_depatamento__(id) {
        $('#img_tipo_departamento').attr($(id).find('div img').attr('src'))
        $('#tipodepartamentodes__').text($(id).find('h3').text())
        $('#div_contend_tipo_depatamento').removeClass('open')
        $('#tipodepartamento_selected').val($(id).find('h3').text())
    }

    function change_tipo(id) {

        var valor = $(id).attr('valor_tipo_departamento');
        $('#dropdowntipo').text($(id).text());
        data_tipo(valor);

    }


    function data_tipo(id) {

        $.ajax({
            type: "GET",
            url: url + '/departamentos/' + id,
            data: { _token: $('meta[name="csrf-token"]').attr('content') },
            success: function (response) {

                if (response.status == "success") {
                    var data = response.data, img___ = "";
                    $('#tipo_span').text(data.tipodepartamento.tipo);
                    $('#area_total_span').text(data.tipodepartamento.area_total + 'm2');
                    $('#habitaciones_span').text(data.tipodepartamento.habitaciones);
                    $('#bano_span').text(data.tipodepartamento.banos);

                                        img___ += '<div class="owl-carousel owl-modelo__ owl-theme">';
                                        $.each(data.img_planos, function (indexInArray, value) {
                                            img___ += '<div class="item">';
                                            img___ += '<img src="' + url + '/storage/' + value.url_imagen + '" class="img-fluid" alt="">';
                                            img___ += '</div>';
                                        });

                                        img___ += '</div>';

                                        $('#carrusel_').html(img___);

                                        var owl = $('.owl-modelo__');
                                        owl.owlCarousel('destroy');
                                        owl.on('refresh.owl.carousel refreshed.owl.carousel');
                                        owl.owlCarousel({
                                            loop: true,
                                            nav: true,
                                            lazyLoad: true,
                                            responsive: {
                                                0: {
                                                    items: 1
                                                },
                                                600: {
                                                    items: 1
                                                },
                                                1000: {
                                                    items: 1
                                                }
                                            }
                                        });

                };
            }
        });
    }


</script>
