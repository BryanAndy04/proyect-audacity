<style>
    iframe {
        width: 100% !important;
        height: 400px;
    }

    .dropzoneDragArea {
        background-color: #fbfdff;
        border: 1px dashed #c0ccda;
        border-radius: 6px;
        padding: 40px;
        text-align: center;
        margin-bottom: 15px;
        cursor: pointer;
    }

    .dropzone {
        box-shadow: 0px 2px 20px 0px #f2f2f2;
        border-radius: 10px;
    }
</style>

<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Caracteristicas</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="save_changes()" class="btn btn-sm btn-primary">Guardar</a>
        </div>
    </div>
</div>

<div class="card-body">
    <div class="nav-wrapper">
        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab"
                    href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i
                        class="ni ni-cloud-upload-96 mr-2"></i>Vista Panoramica</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab"
                    href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i
                        class="ni ni-building mr-2 "></i>Brochure</a>
            </li>
        </ul>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel"
                    aria-labelledby="tabs-icons-text-1-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url del video </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="url_youtube_vista_panoramica" aria-describedby="basic-addon3"
                                value="{{ $caracteristicas->vistapanoramica }}">
                        </div>
                    </div>
                    <div id='iframe' class="text-center">
                        {!! html_entity_decode($caracteristicas->vistapanoramica) !!}
                    </div>
                </div>

                <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel"
                    aria-labelledby="tabs-icons-text-2-tab">

                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <form action="{{ route('caracteristicas.store') }}" name="form" id="form" method="POST"
                                class="dropzone" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="subtitulo" style="font-size: 13px"><span
                                            style="color: red;">(*)</span><b>Brochure</b></label>
                                    <div id="dropzoneDragArea" class=" dz-default dz-message dropzoneDragArea">
                                        <span>Seleccione Brochure</span>
                                        <div class="dropzone-previews"></div>
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <button type="submit" id="btn_form" class="btn btn-primary">Gruardar</button>
                                </div>

                            </form>
                        </div>

                        <div class="col-md-8 col-lg-8">

                            <div class="text-center">
                                <iframe style="height: 490px !important;width: 100% !important" id='iframe_loft'
                                    src="{{ url('storage') }}/{{ $caracteristicas->brochure }}" frameborder="0"
                                    allowfullscreen></iframe>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        var token = $('meta[name="csrf-token"]').attr('content');

        $('#url_youtube_vista_panoramica').keyup(function () {
            var url_video = $(this).val();
            $('#iframe').html(url_video);
        });


        function save_changes () {

            $.ajax({
                type: "PUT",
                url: "{{ route('caracteristicas.update',1) }}",
                data: { _token : token ,vistapanoramica : $('#url_youtube_vista_panoramica').val() },
                success: function (response) {
                    if ( response.status == "success" ) {

                        var msg = alertify.success('Error');
                        msg.delay(5).setContent('Registro se guardado correctamente.');

                    }
                }
            });

        }

        var token = $('meta[name="csrf-token"]').attr('content');
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone("div#dropzoneDragArea", {
            paramName: "file",
            url: "{{ route('caracteristicas.imagen') }}",
            previewsContainer: 'div.dropzone-previews',
            addRemoveLinks: true,
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            params: {
                _token: token
            },
            init: function () {

                var myDropzone = this;
                $("form[name='form']").submit(function (event) {
                    //Make sure that the form isn't actully being sent.
                    event.preventDefault();
                    URL = $("#form").attr('action');
                    formData = $('#form').serialize();
                    $.ajax({
                        type: 'POST',
                        url: URL,
                        data: formData,
                        success: function (result) {
                            if (result.status == "success") {
                                myDropzone.processQueue();
                            } else {
                                var msg = alertify.error('Error');
                                msg.delay(5).setContent('No se pudo guardar registro');
                            }
                        }
                    });

                });

                this.on('sending', function (file, xhr, formData) {});

                this.on("success", function (file, response) {
                    $('.dropzone-previews').empty();
                    var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se guardado correctamente.');

                });

                this.on("queuecomplete", function () {

                });

                this.on("sendingmultiple", function () {
                });

                this.on("successmultiple", function (files, response) {
                });

                this.on("errormultiple", function (files, response) {
                });
            }
        });

    </script>
