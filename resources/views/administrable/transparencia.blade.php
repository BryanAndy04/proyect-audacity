<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Transparencia Senda</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="save_()" class="btn btn-sm btn-primary">Guardar</a>
        </div>
    </div>
</div>

<div class="card-body">
    <div class="col-lg-12 col-md-12 m-auto ">

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Descripción</label>
            <textarea class="form-control" id="descripcion" cols="20" rows="10"
                style="height: 400px !important;">{{ $transparencia->descripcion }}</textarea>
        </div>
    </div>
</div>


<script type="text/javascript">

    var token = $('meta[name="csrf-token"]').attr('content');

    function save_() {

        $.ajax({
            type: "POST",
            url: "{{ url('/transparencia') }}",
            data: { _token: token, descripcion: $('#descripcion').val() },
            success: function (response) {
                var msg = alertify.success('success');
                msg.delay(5).setContent('Registro se actualizo correctamente.');
                $('a[class="nav-link active"]').click();
            }
        });

    }


</script>
