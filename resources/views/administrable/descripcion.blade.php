<style>
    iframe {
        width: 100% !important;
        height: 400px !important;
     }
</style>

<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Descripción</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="save_changes()" class="btn btn-sm btn-primary">Guardar</a>
        </div>
    </div>
</div>


<div class="card-body">


    <div class="nav-wrapper">
        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab"
                    href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i
                        class="ni ni-cloud-upload-96 mr-2"></i>Video</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab"
                    href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i
                        class="ni ni-building mr-2 "></i>Experiencia 360 - LOFT</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab"
                    href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Experiencia 360 - DUO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab"
                    href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Experiencia 360 - SOCIAL</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-5-tab" data-toggle="tab"
                    href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-5" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Experiencia 360 - ÁREAS COMUNES</a>
            </li>
        </ul>
    </div>

    <div class="card shadow">
        <div class="card-body">
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel"
                    aria-labelledby="tabs-icons-text-1-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url del video </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="url_youtube" aria-describedby="basic-addon3"
                                value="{{ $descripcion->link_video }}">
                        </div>
                    </div>
                    <div id='iframe' class="text-center">
                        {!! html_entity_decode($descripcion->link_video) !!}
                    </div>
                </div>

                <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel"
                    aria-labelledby="tabs-icons-text-2-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="exp_loft_url" aria-describedby="basic-addon3"
                                value="{{ $descripcion->exp_loft_url }}">
                        </div>
                    </div>

                    <div class="text-center">
                        <iframe width="560" id='iframe_loft' height="390" src="{{ $descripcion->exp_loft_url }}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>

                </div>

                <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel"
                    aria-labelledby="tabs-icons-text-3-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="exp_duo_url" aria-describedby="basic-addon3"
                                value="{{ $descripcion->exp_duo_url }}">
                        </div>
                    </div>

                    <div class="text-center">
                        <iframe width="560" id='iframe_duo' height="390" src="{{ $descripcion->exp_duo_url }}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>

                </div>

                <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel"
                    aria-labelledby="tabs-icons-text-4-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="exp_social_url" aria-describedby="basic-addon3"
                                value="{{ $descripcion->exp_social_url }}">
                        </div>
                    </div>

                    <div class="text-center">
                        <iframe id='iframe_social' width="560" height="390" src="{{ $descripcion->exp_social_url }}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>

                </div>

                <div class="tab-pane fade" id="tabs-icons-text-5" role="tabpanel"
                    aria-labelledby="tabs-icons-text-5-tab">
                    <div class="form-group">
                        <label class="form-control-label" for="basic-url">Ingresar url </label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="exp_area_com_url"
                                aria-describedby="basic-addon3" value="{{ $descripcion->exp_area_com_url }}">
                        </div>
                    </div>

                    <div class="text-center">
                        <iframe id='iframe_area' width="560" height="390" src="{{ $descripcion->exp_area_com_url }}"
                            frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>

    $('#url_youtube').keyup(function () {
        var url_video = $(this).val();
        $('#iframe').html(url_video);
    });

    $('#exp_loft_url').keyup(function () {
        var url_loft = $(this).val();
        $('#iframe_loft').attr('src', url_loft);
    });

    $('#exp_duo_url').keyup(function () {
        var url_loft = $(this).val();
        $('#iframe_duo').attr('src', url_loft);
    });

    $('#exp_social_url').keyup(function () {
        var url_loft = $(this).val();
        $('#iframe_social').attr('src', url_loft);
    });


    $('#exp_area_com_url').keyup(function () {
        var url_loft = $(this).val();
        $('#iframe_area').attr('src', url_loft);
    });


    function save_changes() {

        $.ajax({

            type: "POST",
            url: "{{ route('descripcion.store') }}",
            data: {
                _token : $('meta[name="csrf-token"]').attr('content'),
                url_youtube: $('#url_youtube').val(),
                url_loft: $('#exp_loft_url').val(),
                exp_duo_url: $('#exp_duo_url').val(),
                exp_social_url: $('#exp_social_url').val(),
                exp_area_com_url: $('#exp_area_com_url').val()
            },
            success: function (response) {

                if (response.status == "success") {
                    var msg = alertify.success('Success');
                    msg.delay(5).setContent('Registro se guardado correctamente.');
                }

            }

        });



    }



</script>
