<style>
    .dropzoneDragArea {
        background-color: #fbfdff;
        border: 1px dashed #c0ccda;
        border-radius: 6px;
        padding: 40px;
        text-align: center;
        margin-bottom: 15px;
        cursor: pointer;
    }

    .dropzone {
        box-shadow: 0px 2px 20px 0px #f2f2f2;
        border-radius: 10px;
    }
</style>

<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Inico</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="add_()" class="btn btn-sm btn-primary">Agregar</a>
        </div>
    </div>
</div>

<div class="table-responsive">
    <div>
        <table class="table align-items-center">
            <thead class="thead-light">
                <tr>
                    <th scope="col" class="sort" data-sort="titulo">Título</th>
                    <th scope="col" class="sort" data-sort="adicional">Título en verde</th>
                    <th scope="col" class="sort" data-sort="tipo">Tipo</th>
                    <th scope="col" class="sort" data-sort="descripcion">Descripción</th>
                    <th scope="col" class="sort" data-sort="orden">Orden</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="list">

                @if ( count($data) > 0 )
                @foreach ( $data as $row)
                <tr>
                    <th scope="row">
                        <div class="media align-items-center">
                            <a href="#" class="avatar rounded-circle mr-3">
                                <img alt="Image placeholder" src="{{ url('storage') }}/{{ $row->ruta_imagen }}">
                            </a>
                            <div class="media-body">
                                <span class="name mb-0 text-sm">{{ ucfirst(strtolower($row->title)) }}</span>
                            </div>
                        </div>
                    </th>
                    <td class="budget">{{ ucfirst(strtolower($row->title_personalizado)) }}</td>
                    <td class="budget">{{ ucfirst(strtolower($row->tipo_subtitulo)) }}</td>
                    <td class="budget">{{ ucfirst(strtolower($row->subtitulo)) }}</td>
                    <td class="budget">{{ ucfirst(strtolower($row->orden)) }}</td>
                    <td class="text-right">
                        <div class="dropdown">
                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"  onclick="update_(this)">Editar</a>
                                <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"  onclick="delete_(this)">Eliminar</a>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <th colspan="6">
                        <center>No hay registros</center>
                    </th>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>



<div class="modal fade" id="inicio_modal" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-wrapper">
                    <form action="{{ route('inicio.store') }}" name="form" id="form" method="POST" class="dropzone"
                        enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" class="inicioid" name="inicioid" id="inicioid" value="">
                        <input type="hidden" class="updtae_" name='updtae_' id='updtae_' value='0'>

                        <div class="form-group">
                            <label for="title" style="font-size: 13px"><span style="color: red;">(*)</span><b>Título
                                </b></label>
                            <input type="text" id='title' class="form-control form-control-alternative"
                                placeholder="Título" name='title' required>
                        </div>

                        <div class="form-group">
                            <label for="title_personalizado" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Adicionar al título</b></label>
                            <input type="text" id='title_personalizado' class="form-control form-control-alternative"
                                placeholder="Adicionar al título" name='title_personalizado' required>
                        </div>

                        <div class="form-group">
                            <label for="tipo_subtitulo" style="font-size: 13px"><b>Tipo de Subtítulo</b></label>
                            <select class="form-control form-control-alternative" id="tipo_subtitulo"
                                name='tipo_subtitulo'>
                                <option value='descripcion'>Descripción</option>
                                <option value='direccion'>Dirección</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="subtitulo" style="font-size: 13px"><b>Subtítulo</b></label>
                            <input type="text" id='subtitulo' class="form-control form-control-alternative"
                                placeholder="Subtítulo" name='subtitulo'>
                        </div>

                        <div class="form-group">
                            <label for="orden" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Orden</b></label>
                            <select class="form-control form-control-alternative" id="orden" name='orden' required>
                                <option value=''>Seleccione</option>
                                <option value='1'>1</option>
                                <option value='2'>2</option>
                                <option value='3'>3</option>
                                <option value='4'>4</option>
                                <option value='5'>5</option>
                                <option value='6'>6</option>
                                <option value='7'>7</option>
                                <option value='8'>8</option>
                                <option value='9'>9</option>
                                <option value='10'>10</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="subtitulo" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Imagen</b></label>

                            <div class="text-center"><img src="" style="display: none;width: 120px;margin-bottom: 10px;" id="img__"></div>

                            <div id="dropzoneDragArea" class="dz-default dz-message dropzoneDragArea">
                                <span>Seleccione imagen</span>
                                <div class="dropzone-previews"></div>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_form" class="btn btn-primary">Gruardar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
   var token = $('meta[name="csrf-token"]').attr('content');
    function add_() {
        $('#img__').fadeOut();
        $("#exampleModalLabel").html('Agregar registro');
        $('#inicio_modal').modal('show');
        $('#updtae_').val('0');
        $('#form')[0].reset();
        $('.dropzone-previews').empty();
    }

    function delete_(id){
        var id_ = $(id).attr('id_');
        $.ajax({
            type: "DELETE",
            url: "{{ url('inicio') }}/"+id_,
            data: { _token : token},
            success: function (response) {

                if( response.status == "success" ) {
                    var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se elimino correctamente.');
                    $('a[class="nav-link active"]').click();
                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo eliminar registro');
                }


            }
        });
    }

    function update_(id){

        var id_ = $(id).attr('id_');
        $.ajax({
            type: 'GET',
            url: "{{ url('inicio') }}/"+id_,
            data: { _token : token },
            dataType: "json",
            success: function (response) {


                if ( response.status == "success" ) {

                   var data = response.data;

                   $('#inicioid').val(data.id);
                   $('#title').val(data.title);
                   $('#title_personalizado').val(data.title_personalizado);
                   $('#tipo_subtitulo').val(data.tipo_subtitulo);
                   $('#subtitulo').val(data.subtitulo);
                   $('#orden').val(data.orden);
                   $('#img__').attr('src', "{{ url('storage') }}/"+data.ruta_imagen).fadeIn();
                   $('#updtae_').val('1');

                   myDropzone.processQueue();

                   $("#exampleModalLabel").html('Editar registro');
                   $('#inicio_modal').modal('show');

                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo obtener información del registro');
                }
            }
        });

    }

    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone("div#dropzoneDragArea", {
        paramName: "file",
        url: "{{ route('inicio.imagen') }}",
        previewsContainer: 'div.dropzone-previews',
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        params: {
            _token: token
        },
        init: function () {

            var myDropzone = this;
            $("form[name='form']").submit(function (event) {

                //Make sure that the form isn't actully being sent.
                event.preventDefault();

                URL = $("#form").attr('action');
                formData = $('#form').serialize();
                $.ajax({
                    type: 'POST',
                    url: URL,
                    data: formData,
                    success: function (result) {
                        if (result.status == "success") {
                            var inicioid = result.inicioid;
                            $("#inicioid").val(inicioid);
                            myDropzone.processQueue();
                            if ( $('#updtae_').val() == '1' ) {
                               $('#inicio_modal').modal('hide');
                               $('a[class="nav-link active"]').click();
                            }

                        } else {
                            var msg = alertify.error('Error');
                            msg.delay(5).setContent('No se pudo guardar registro');
                        }
                    }
                });

            });

            this.on('sending', function (file, xhr, formData) {
                var inicioid = document.getElementById('inicioid').value;
                formData.append('inicioid', inicioid);
            });

            this.on("success", function (file, response) {
                $('#form')[0].reset();
                $('.dropzone-previews').empty();
                $('#inicio_modal').modal('hide');
                $('a[class="nav-link active"]').click();
                $('#updtae_').val('0');

                var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se guardado correctamente.');

            });

            this.on("queuecomplete", function () {

            });

            this.on("sendingmultiple", function () {
            });

            this.on("successmultiple", function (files, response) {
            });

            this.on("errormultiple", function (files, response) {
            });
        }
    });

</script>
