<style>
    .dropzoneDragArea {
        background-color: #fbfdff;
        border: 1px dashed #c0ccda;
        border-radius: 6px;
        padding: 40px;
        text-align: center;
        margin-bottom: 15px;
        cursor: pointer;
    }

    .dropzone {
        box-shadow: 0px 2px 20px 0px #f2f2f2;
        border-radius: 10px;
    }
    .cropper-crop {
        display: none;
    }
    .cropper-bg {
        background: none;
    }

</style>

<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Galería</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="save_position()" class="btn btn-sm btn-primary">Guardar</a>
            <a href="javascript:void(0);"  onclick="add_()" class="btn btn-sm btn-primary">Agregar</a>
        </div>
    </div>
</div>

<div class="card-body">
    <div class="col-lg-12 col-md-12 m-auto">
        <div class="listado filter">
            <a href="javascript:void(0)" data-filter="*" class="current">TODOS</a>
            <a href="javascript:void(0)" data-filter=".exteriores">exteriores</a>
            <a href="javascript:void(0)" data-filter=".interiores">interiores</a>
            <a href="javascript:void(0)" data-filter=".area_comunes">Áreas comunes</a>
        </div>
    </div>

    <div class="grid">
        <div class="grid-sizer"></div>
        @if (count($galeria) > 0 )
        @foreach ($galeria as $rowimage )
        <div class="grid-item {{ $rowimage->categoria }}" orden="{{ $loop->iteration }}" valor__ ="{{ $rowimage->id }}">
            <a href="javascript:void(0)">
                <img id="img___{{ $rowimage->id}}" src="{{ url('storage') }}/{{ $rowimage->link_imagen}}" style="display:none">
                <img class="card-img-top"  src="@if( strlen($rowimage->link_imagen_cropped) > 10 ){{ url('storage') }}/{{ $rowimage->link_imagen_cropped}}@else{{ url('storage') }}/{{ $rowimage->link_imagen}} @endif" alt="Card image cap">
                <div class="grid__magnifier">
                    <div class="grid__tp-info">
                        <i class="fa fa-trash "
                     onclick="eliminar({{ $rowimage->id }})"></i>
                    <i class="fa fa-cut"
                      onclick="cortar_imagen({{ $rowimage->id }})"></i>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
        @else
        <div class="col-lg-12 text-center">
            <div class="sinImagen">
                <h5>No hay imagenes para esta categoría</h5>
            </div>
        </div>
        @endif
    </div>



</div>

<div class="modal fade" id="inicio_modal" data-backdrop="static" role="dialog" aria-labelledby="inicio_modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id='cerrar_modal'>
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-wrapper">
                    <form action="{{ route('galeria.store') }}" name="form" id="form" method="POST" class="dropzone"
                        enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="id_galeria" id='id_galeria'>

                        <div class="form-group">
                            <label for="categoria" style="font-size: 13px"><b>Seleccione Filtro</b></label>
                            <select class="form-control form-control-alternative" id="categoria" name='categoria'
                                required>
                                <option value=''>Seleccione</option>
                                <option value='exteriores'>Exteriores</option>
                                <option value='interiores'>Interiores</option>
                                <option value='area_comunes'>Áreas comunes</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="subtitulo" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Imagen</b></label>

                            <div id="dropzoneDragArea" class=" dz-default dz-message dropzoneDragArea">
                                <span>Seleccione Imagen</span>
                                <div class="dropzone-previews"></div>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button type="button" class="btn btn-link  ml-auto" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_form" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal- modal-dialog-centered modal-" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="modal-title-default">Cortar imagen</h4>
            </div>

            <div class="modal-body">
                <div class="img-container">
                    <img id="image" class="img-fluid" src=""
                    alt="Picture">
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="save_img()">Guardar</button>
                <button type="button" class="btn btn-link  ml-auto" onclick="cerrar_modal_recorte()">Cerrar</button>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    var cropper;
    var id_selected;

    var $grid = $('.grid').packery({
        itemSelector: '.grid-item',
        stamp: '.stamp',
        percentPosition: true,
        resize: false,
        columnWidth: 1
    });

    // make all items draggable
    var $items = $grid.find('.grid-item').draggable();
    // bind drag events to Packery
    $grid.packery('bindUIDraggableEvents', $items);

    function orderItems() {

        var itemElems = $grid.packery('getItemElements');
        $(itemElems).each(function (i, itemElem) {
            $(itemElems).each(function (i, itemElem) {
                $(itemElem).attr('orden', i + 1);
            });
        });
    }

    $grid.on('layoutComplete', orderItems);

    //$grid.on('dragItemPositioned', orderItems);
    $grid.on('dragItemPositioned', function (event, draggedItem) {
        console.log('Packery drag item positioned',
            draggedItem.element);
    });



    $('.filter a').click(function () {
        $('.filter .current').removeClass('current');
        $(this).addClass('current');

        var selector = $(this).data('filter');
        $('.grid').isotope({
            filter: selector
        });
        return false;
    });


    $('#categoria').val('exteriores');

    $('a[role="tab"]').click(function () {
        var valor = $(this).attr('valor');
        $('#categoria').val(valor);
    })

    function add_() {
        $('#inicio_modal').modal('show');
    }

    function cerrar_modal_recorte () {
        $('#modal').modal('hide');
        cropper.destroy();
        cropper = null;
    }

    function save_img() {

        var canvas;
        if ( cropper ) {

            canvas = cropper.getCroppedCanvas();
            canvas.toBlob(function (blob) {

                var formData = new FormData();
                formData.append('_token',token);
                formData.append('id_galeria', id_selected);
                formData.append('file',blob);

                $.ajax({
                    url: "{{ url('/recorteimage') }}", // name of the file which we will be creating soon
                    method: "POST",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        cerrar_modal_recorte()
                        var msg = alertify.success('Error');
                        msg.delay(5).setContent('Registro se elimino correctamente.');
                        setTimeout(function () {
                            $('a[class="nav-link active"]').click();
                        },1000)

                    }
                });


            })
        }

    }



    function cortar_imagen(id_img) {
        id_selected = id_img;
        var image__ = $('#img___' + id_img).attr('src');
        $('#image').attr('src', image__);
        var Image = document.getElementById('image');
        setTimeout(function () {
            cropper = new Cropper(Image, {
                zoomable: false
            });
        }, 2000)
        $('#modal').modal('show');
    }

    function eliminar(id) {
        $.ajax({
            type: "DELETE",
            url: "{{ url('galeria') }}/" + id,
            data: { _token: token, id: id },
            success: function (response) {
                if (response.status == "success") {
                    var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se elimino correctamente.');
                    $('a[class="nav-link active"]').click();
                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo eliminar registro');
                }


            }
        });
    }



    Dropzone.autoDiscover = false;
    var data = [];
    var position__ = new Object();
    var token = $('meta[name="csrf-token"]').attr('content');
    var myDropzone = new Dropzone("div#dropzoneDragArea", {
        paramName: "file",
        url: "{{ route('galeria.imagen') }}",
        previewsContainer: 'div.dropzone-previews',
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        params: {
            _token: token
        },
        init: function () {

            var myDropzone = this;
            $("form[name='form']").submit(function (event) {

                //Make sure that the form isn't actully being sent.
                event.preventDefault();

                URL = $("#form").attr('action');
                formData = $('#form').serialize();
                $.ajax({
                    type: 'POST',
                    url: URL,
                    data: formData,
                    success: function (result) {
                        if (result.status == "success") {
                            var id_galeria = result.id_galeria;
                            $("#id_galeria").val(id_galeria);
                            $('#inicio_modal').modal('hide');
                            myDropzone.processQueue();
                        } else {
                            var msg = alertify.error('Error');
                            msg.delay(5).setContent('No se pudo guardar registro');
                        }
                    }
                });

            });

            this.on('sending', function (file, xhr, formData) {
                var id_galeria = document.getElementById('id_galeria').value;
                formData.append('id_galeria', id_galeria);
            });

            this.on("success", function (file, response) {
                $('#form')[0].reset();
                $('.dropzone-previews').empty();
                $('a[class="nav-link active"]').click();
                var msg = alertify.success('Error');
                msg.delay(5).setContent('Registro se guardado correctamente.');
            });

            this.on("queuecomplete", function () {

            });

            this.on("sendingmultiple", function () {
            });

            this.on("successmultiple", function (files, response) {
            });

            this.on("errormultiple", function (files, response) {
            });
        }
    });


    function orderItems() {
        var itemElems = $grid.packery('getItemElements');
        $(itemElems).each(function (i, itemElem) {
            $(itemElems).each(function (i, itemElem) {
                $(itemElem).attr('orden', i + 1);
            });
        });
    }

    $grid.on('layoutComplete', orderItems);

    //$grid.on('dragItemPositioned', orderItems);
    $grid.on('dragItemPositioned', function (event, draggedItem) {
        console.log('Packery drag item positioned',
            draggedItem.element);
    });

    function save_position() {
        position__.length = 0;
        var arr_, style__;
        data.length = 0;
        $('.grid').find('div.grid-item').each(function () {
            arr = $(this).attr('style').split(';');
            style__ = arr[0] + ";" + arr[1] + ";" + arr[2];
            data.push({ style: style__, orden: $(this).attr('orden'), id: $(this).attr('valor__') });
        });

        position__.data = data;
        console.log(JSON.stringify(position__));

        $.ajax({
            type: "post",
            url: "{{ url('/galeriapostion') }}",
            data: { params: JSON.stringify(position__), _token: token },
            success: function (response) {
                var msg = alertify.success('Error');
                msg.delay(5).setContent('Registros se actulizaron correctamente.');
                $('a[class="nav-link active"]').click();
            }
        });
    }

    var $grid = $('.grid').imagesLoaded(function () {
        $grid.masonry({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer'
        });
    });

</script>
