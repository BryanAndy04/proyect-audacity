<style>
    .dropzoneDragArea {
        background-color: #fbfdff;
        border: 1px dashed #c0ccda;
        border-radius: 6px;
        padding: 40px;
        text-align: center;
        margin-bottom: 15px;
        cursor: pointer;
    }

    .dropzone {
        box-shadow: 0px 2px 20px 0px #f2f2f2;
        border-radius: 10px;
    }
</style>

<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Transparencia Senda</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="add()" class="btn btn-sm btn-primary">Agregar</a>
        </div>
    </div>
</div>

<div class="card-body">

    <div class="nav-wrapper">

        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab"
                    href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i
                        class="ni ni-building mr-2"></i>Loft</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab"
                    href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Duo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab"
                    href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Social</a>
            </li>
            <li class="nav-item">
                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab"
                    href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i
                        class="ni ni-building mr-2"></i>Unico</a>
            </li>
        </ul>

    </div>


    <div class="tab-content" id="myTabContent">

        <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel"
            aria-labelledby="tabs-icons-text-1-tab">

            <div class="table-responsive">
                <div>
                    <table class="table align-items-center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="tipo">Tipo</th>
                                <th scope="col" class="sort" data-sort="adicional">Área total</th>
                                <th scope="col" class="sort" data-sort="tipo">Número de Habitaciones</th>
                                <th scope="col" class="sort" data-sort="descripcion">Número de Baños</th>
                                <th scope="col" class="sort" data-sort="orden">Plano</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            @if ( count($loft) > 0 )
                            @foreach ( $loft as $row)
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder"
                                                src="{{ url('storage') }}/{{ $row->img_principal }}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{ $row->tipo }}</span>
                                        </div>
                                    </div>
                                </th>
                                <td class="budget">{{ $row->area_total }}</td>
                                <td class="budget">{{ $row->habitaciones }}</td>
                                <td class="budget">{{ $row->banos }}</td>
                                <td class="icono-morado"><i class="ni ni-image" onclick="ver_imagenes({{ $row->id }})"
                                        style="font-size: 13px;cursor: pointer;"></i></td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="update_(this)">Editar</a>
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="delete_(this)">Eliminar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="5">
                                    <center>No hay registros</center>
                                </th>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>


        </div>

        <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">

            <div class="table-responsive">
                <div>
                    <table class="table align-items-center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="tipo">Tipo</th>
                                <th scope="col" class="sort" data-sort="adicional">Área total</th>
                                <th scope="col" class="sort" data-sort="tipo">Número de Habitaciones</th>
                                <th scope="col" class="sort" data-sort="descripcion">Número de Baños</th>
                                <th scope="col" class="sort" data-sort="orden">Plano</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            @if ( count($duo) > 0 )
                            @foreach ( $duo as $row)
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder"
                                                src="{{ url('storage') }}/{{ $row->img_principal }}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{ $row->tipo }}</span>
                                        </div>
                                    </div>
                                </th>
                                <td class="budget">{{ $row->area_total }}</td>
                                <td class="budget">{{ $row->habitaciones }}</td>
                                <td class="budget">{{ $row->banos }}</td>
                                <td class="icono-morado"><i class="ni ni-image" onclick="ver_imagenes({{ $row->id }})"
                                        style="font-size: 13px;cursor: pointer;"></i></td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="update_(this)">Editar</a>
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="delete_(this)">Eliminar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="5">
                                    <center>No hay registros</center>
                                </th>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>


        </div>

        <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">

            <div class="table-responsive">
                <div>
                    <table class="table align-items-center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="tipo">Tipo</th>
                                <th scope="col" class="sort" data-sort="adicional">Área total</th>
                                <th scope="col" class="sort" data-sort="tipo">Número de Habitaciones</th>
                                <th scope="col" class="sort" data-sort="descripcion">Número de Baños</th>
                                <th scope="col" class="sort" data-sort="orden">Plano</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            @if ( count($social) > 0 )
                            @foreach ( $social as $row)
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder"
                                                src="{{ url('storage') }}/{{ $row->img_principal }}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{ $row->tipo }}</span>
                                        </div>
                                    </div>
                                </th>
                                <td class="budget">{{ $row->area_total }}</td>
                                <td class="budget">{{ $row->habitaciones }}</td>
                                <td class="budget">{{ $row->banos }}</center>
                                </td>
                                <td class="icono-morado"><i class="ni ni-image" onclick="ver_imagenes({{ $row->id }})"
                                    style="font-size: 13px;cursor: pointer;"></i></td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="update_(this)">Editar</a>
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="delete_(this)">Eliminar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="5">
                                    <center>No hay registros</center>
                                </th>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">

            <div class="table-responsive">
                <div>
                    <table class="table align-items-center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="tipo">Tipo</th>
                                <th scope="col" class="sort" data-sort="adicional">Área total</th>
                                <th scope="col" class="sort" data-sort="tipo">Número de Habitaciones</th>
                                <th scope="col" class="sort" data-sort="descripcion">Número de Baños</th>
                               <th scope="col" class="sort" data-sort="orden">Plano</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody class="list">

                            @if ( count($unico) > 0 )
                            @foreach ( $unico as $row)
                            <tr>
                                <th scope="row">
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder"
                                                src="{{ url('storage') }}/{{ $row->img_principal }}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{ $row->tipo }}</span>
                                        </div>
                                    </div>
                                </th>
                                <td class="budget">{{ $row->area_total }}</td>
                                <td class="budget">{{ $row->habitaciones }}</td>
                                <td class="budget">{{ $row->banos }}</td>
                                <td class="icono-morado"><i class="ni ni-image" onclick="ver_imagenes({{ $row->id }})"
                                        style="font-size: 13px;cursor: pointer;"></i></td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="update_(this)">Editar</a>
                                            <a class="dropdown-item" href="javascript:void(0);" id_="{{ $row->id }}"
                                                onclick="delete_(this)">Eliminar</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="5">
                                    <center>No hay registros</center>
                                </th>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="modal fade modal__grande" id="images__" data-backdrop="static" role="dialog" aria-labelledby="images__"
    aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-title-default" id='title__'>Planos</h5>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-md-4">
                        <div>
                            <p
                                style="font-size: 13px !important;margin-bottom: 4px !important;font-weight: 16 !important; ">
                                <b>Categoría : </b> <span id="categoria_span"></span></p>
                            <p
                                style="font-size: 13px !important;margin-bottom: 4px !important;font-weight: 16 !important; ">
                                <b>Tipo : </b> <span id="tipo_span"></span></p>
                            <p
                                style="font-size: 13px !important;margin-bottom: 4px !important;font-weight: 16 !important; ">
                                <b>Área Total : </b> <span id="area_total_span"></span></p>
                            <p
                                style="font-size: 13px !important;margin-bottom: 4px !important;font-weight: 16 !important; ">
                                <b>N° de Habitaciones : </b> <span id="habitaciones_span"></span></p>
                            <p
                                style="font-size: 13px !important;margin-bottom: 4px !important;font-weight: 16 !important; ">
                                <b>N° de Baños : </b> <span id="bano_span"></span></p>
                            <br>

                            <form action="{{ route('departamentos.store.planos') }}" name="form_2" id="form_2"
                                method="POST" class="dropzone" enctype="multipart/form-data">

                                @csrf

                                <input type="hidden" class="inicioid_" name="inicioid_" id="inicioid_" value="">
                                <input type="hidden" class="imagenid_" name="imagenid_" id="imagenid_" value="">
                                <div class="form-group">
                                    <label for="subtitulo" style="font-size: 13px"><span
                                            style="color: red;">(*)</span><b>Adjuntar Imagen</b></label>

                                    <div class="text-center">

                                        <div id="dropzoneDragArea2" class="dz-default dz-message dropzoneDragArea">
                                            <span>Seleccione imagen</span>
                                            <div class="dropzone-previews2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="button" class="btn btn-link  ml-auto"
                                        data-dismiss="modal">Cerrar</button>
                                    <button type="submit" id="btn_form__" class="btn btn-primary">Guardar</button>
                                </div>

                            </form>

                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row" id='cesta_planos'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="inicio_modal" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-wrapper">
                    <form action="{{ route('departamentos.store') }}" name="form" id="form" method="POST"
                        class="dropzone" enctype="multipart/form-data">

                        @csrf

                        <input type="hidden" class="inicioid" name="inicioid" id="inicioid" value="">
                        <input type="hidden" class="updtae_" name='updtae_' id='updtae_' value='0'>

                        <div class="form-group">
                            <label for="categoria" style="font-size: 13px"><b>Categoría</b></label>
                            <select class="form-control form-control-alternative" id="categoria" name='categoria'
                                required>
                                <option value=''>Seleccione</option>
                                <option value='loft'>Loft</option>
                                <option value='duo'>Duo</option>
                                <option value='social'>Social</option>
                                <option value='unico'>Único</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tipo" style="font-size: 13px"><span style="color: red;">(*)</span><b>Tipo
                                </b></label>
                            <input type="text" id='tipo' class="form-control form-control-alternative"
                                placeholder="Tipo" name='tipo' required>
                        </div>

                        <div class="form-group">
                            <label for="area_total" style="font-size: 13px"><span style="color: red;">(*)</span><b>Área
                                    total</b></label>
                            <input type="text" id='area_total' class="form-control form-control-alternative"
                                placeholder="Área total (m2) " name='area_total' required>
                        </div>

                        <div class="form-group">
                            <label for="habitaciones" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Número de habitaciones</b></label>
                            <input type="text" id='habitaciones' class="form-control form-control-alternative"
                                placeholder="Número de habitaciones" name='habitaciones'>
                        </div>

                        <div class="form-group">
                            <label for="banos" style="font-size: 13px"><span style="color: red;">(*)</span><b>Número de
                                    baños</b></label>
                            <input type="text" id='banos' class="form-control form-control-alternative"
                                placeholder="Número de baños" name='banos'>
                        </div>


                        <div class="form-group">
                            <label for="subtitulo" style="font-size: 13px"><span
                                    style="color: red;">(*)</span><b>Plano</b></label>

                            <div class="text-center"><img src="" style="display: none;width: 120px;margin-bottom: 10px;"
                                    id="img__"></div>

                            <div id="dropzoneDragArea" class="dz-default dz-message dropzoneDragArea">
                                <span>Seleccione imagen</span>
                                <div class="dropzone-previews"></div>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="btn_form_" class="btn btn-primary">Guardar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var token = $('meta[name="csrf-token"]').attr('content');
    var id_tipo;

    function add() {
        $('#img__').fadeOut();
        $("#exampleModalLabel").html('Agregar registro');
        $('#inicio_modal').modal('show');
        $('#updtae_').val('0');
        $('#form')[0].reset();
        $('.dropzone-previews').empty();
    }


    function ver_imagenes(id) {

        $.ajax({
            type: 'GET',
            url: "{{ url('departamentos') }}/" + id,
            data: { _token: token },
            dataType: "json",
            success: function (response) {

                if (response.status == "success") {
                    var data = response.data;

                    $('#inicioid_').val(data.tipodepartamento.id);
                    $('#categoria_span').text(data.tipodepartamento.categoria);
                    $('#tipo_span').text(data.tipodepartamento.tipo);
                    $('#area_total_span').text(data.tipodepartamento.area_total);
                    $('#habitaciones_span').text(data.tipodepartamento.habitaciones);
                    $('#bano_span').text(data.tipodepartamento.banos);
                    $('#images__').modal('show');
                    mostrar_planos();

                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo obtener información del registro');
                }
            }
        });


    }



    function mostrar_planos() {

        var storage_ = "{{ url('storage') }}/";

        $.get("{{ url('departamentos') }}/" + $('#inicioid_').val(), { _token: token }, function (response) {
            var data = response.data,
                img___ = "";

            if (data.img_planos.length > 0) {
                $.each(data.img_planos, function (indexInArray, value) {
                    img___ += '<div class="col-lg-3 col-md-4 mt-2"><div class="card card--departamento">';
                    img___ += '<img class="card-img-top" src="' + storage_ + value.url_imagen + '" alt="Card image cap">';
                    img___ += '<div class="card-body borrar text-right">';
                    img___ += '<button class="btn btn-icon btn-primary" type="button" onclick="eliminar_img_plano(' + value.id + ')"><span class="btn-inner--icon"><i class="fa fa-trash"></i></span></button>';
                    img___ += '</div>';
                    img___ += '</div>';
                    img___ += '</div>';
                });

            } else {
                img___ += '<div class="col-lg-12 col-md-12 mt-12 text-center">No se encontro Planos</div>';
            }

            $('#cesta_planos').html(img___);
        });

    }


    function eliminar_img_plano(id) {

        $.ajax({
            type: "POST",
            url: "{{ route('departamentos.imagen.delete.planos') }}",
            data: { _token: token, id: id },
            success: function (response) {

                var msg = alertify.success('Error');
                msg.delay(5).setContent('Registro se elimino correctamente.');
                mostrar_planos();

            }
        });

    }

    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone("div#dropzoneDragArea", {
        paramName: "file",
        url: "{{ route('departamentos.imagen') }}",
        previewsContainer: 'div.dropzone-previews',
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: false,
        parallelUploads: 1,
        maxFiles: 1,
        params: {
            _token: token
        },
        init: function () {

            var myDropzone = this;
            $("form[name='form']").submit(function (event) {

                //Make sure that the form isn't actully being sent.
                event.preventDefault();

                URL = $("#form").attr('action');
                formData = $('#form').serialize();
                $.ajax({
                    type: 'POST',
                    url: URL,
                    data: formData,
                    success: function (result) {
                        if (result.status == "success") {
                            var inicioid = result.inicioid;
                            $("#inicioid").val(inicioid);
                            myDropzone.processQueue();
                            if ($('#updtae_').val() == '1') {
                                $('#inicio_modal').modal('hide');
                                $('a[class="nav-link active"]').click();
                            }

                        } else {
                            var msg = alertify.error('Error');
                            msg.delay(5).setContent('No se pudo guardar registro');
                        }
                    }
                });

            });

            this.on('sending', function (file, xhr, formData) {
                var inicioid = document.getElementById('inicioid').value;
                formData.append('inicioid', inicioid);
            });

            this.on("success", function (file, response) {
                $('#form')[0].reset();
                $('.dropzone-previews').empty();
                $('#inicio_modal').modal('hide');
                setTimeout(function () {

                    $('a[class="nav-link active"]').click();
                    $('#updtae_').val('0');

                }, 1000)


                var msg = alertify.success('Error');
                msg.delay(5).setContent('Registro se guardado correctamente.');

            });

            this.on("queuecomplete", function () {

            });

            this.on("sendingmultiple", function () {
            });

            this.on("successmultiple", function (files, response) {
            });

            this.on("errormultiple", function (files, response) {
            });
        }
    });


    var myDropzone2 = new Dropzone("div#dropzoneDragArea2", {
        paramName: "file",
        url: "{{ route('departamentos.imagen.planos') }}",
        previewsContainer: 'div.dropzone-previews2',
        addRemoveLinks: true,
        autoProcessQueue: false,
        uploadMultiple: false,
        params: {
            _token: token
        },
        init: function () {

            var myDropzone2 = this;
            $("form[name='form_2']").submit(function (event) {

                //Make sure that the form isn't actully being sent.
                event.preventDefault();

                URL = $("#form_2").attr('action');
                formData = $('#form_2').serialize();
                $.ajax({
                    type: 'POST',
                    url: URL,
                    data: formData,
                    success: function (result) {
                        if (result.status == "success") {
                            var imagenid_ = result.inicioid;
                            $("#imagenid_").val(imagenid_);
                            myDropzone2.processQueue();
                        } else {
                            var msg = alertify.error('Error');
                            msg.delay(5).setContent('No se pudo guardar registro');
                        }
                    }
                });

            });

            this.on('sending', function (file, xhr, formData) {
                var inicioid = document.getElementById('imagenid_').value;
                formData.append('imagenid_', inicioid);
            });

            this.on("success", function (file, response) {

                setTimeout(function () {

                    $('.dropzone-previews2').empty();
                    var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se guardado correctamente.');
                    mostrar_planos();

                },1000)


            });

            this.on("queuecomplete", function () {

            });

            this.on("sendingmultiple", function () {
            });

            this.on("successmultiple", function (files, response) {
            });

            this.on("errormultiple", function (files, response) {
            });
        }
    });



    function delete_(id) {
        var id_ = $(id).attr('id_');
        $.ajax({
            type: "DELETE",
            url: "{{ url('departamentos') }}/" + id_,
            data: { _token: token },
            success: function (response) {

                if (response.status == "success") {
                    var msg = alertify.success('Error');
                    msg.delay(5).setContent('Registro se elimino correctamente.');
                    $('a[class="nav-link active"]').click();
                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo eliminar registro');
                }


            }
        });
    }

    function update_(id) {

        var id_ = $(id).attr('id_');
        $.ajax({
            type: 'GET',
            url: "{{ url('departamentos') }}/" + id_,
            data: { _token: token },
            dataType: "json",
            success: function (response) {

                if (response.status == "success") {

                    var data = response.data.tipodepartamento;
                    $('#inicioid').val(data.id);
                    $('#categoria').val(data.categoria);
                    $('#tipo').val(data.tipo);
                    $('#area_total').val(data.area_total);
                    $('#habitaciones').val(data.habitaciones);
                    $('#banos').val(data.banos);
                    $('#img__').attr('src', "{{ url('storage') }}/" + data.img_principal).fadeIn();
                    $('#updtae_').val('1');

                    myDropzone.processQueue();

                    $("#exampleModalLabel").html('Editar registro');
                    $('#inicio_modal').modal('show');

                } else {
                    var msg = alertify.error('Error');
                    msg.delay(5).setContent('No se pudo obtener información del registro');
                }
            }
        });

    }


</script>
