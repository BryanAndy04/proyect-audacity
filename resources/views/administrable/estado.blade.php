<div class="card-header border-0">
    <div class="row align-items-center">
        <div class="col">
            <h3 class="mb-0">Estado</h3>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="activar()" class="btn btn-sm btn-primary">Activar seccion</a>
        </div>
        <div class="col text-right">
            <a href="javascript:void(0);" onclick="add()" class="btn btn-sm btn-primary">Agregar</a>
        </div>
    </div>
</div>
<div class="card-body">

    <div class="table-responsive">
        <div>
            <table class="table align-items-center">
                <thead class="thead-light">
                    <tr>
                        <th scope="col" class="sort" data-sort="Fecha">Fecha</th>
                        <th scope="col" class="sort" data-sort="Porcentaje">Porcentaje</th>
                        <th scope="col" class="sort" data-sort="Galeria">Galeria</th>
                        <th scope="col" class="sort" data-sort=""></th>
                    </tr>
                </thead>
                <tbody class="list">
                       <tr>
                           <td colspan="4"><center>No hay resultados</center></td>
                       </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
