window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    var jQueryBridget = require('jquery-bridget');
    window.AOS = require('AOS');
    AOS.init();
    window.owlcarousel = require('owl.carousel');
    require('@fortawesome/fontawesome-free');
    window.Popper = require('popper.js').default;
    require('bootstrap');
    window.Masonry = require('masonry-layout');
    window.ImagesLoaded = require('imagesloaded');
    require('@fancyapps/fancybox');
    require('isotope-layout');

    jQueryBridget( 'owlCarousel', owlcarousel, $ );

} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
