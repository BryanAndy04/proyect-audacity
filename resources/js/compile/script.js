$(function() {

  //
  // var $grid = $('.grid').imagesLoaded( function() {
  //   $grid.masonry({
  //     itemSelector: '.grid-item',
  //     percentPosition: true,
  //     columnWidth: '.grid-sizer'
  //   });
  // });

  $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
  });

  $('.filter a').click(function(){
   $('.filter .current').removeClass('current');
   $(this).addClass('current');

   var selector = $(this).data('filter');
   $('.grid').isotope({
     filter: selector
   });
   return false;
 });

  $('.asesorateI').click(function() {
    $('#asesorateF').addClass('openA');
  });

  $('.cerrarA').click(function() {
    $('#asesorateF').removeClass('openA');
  });


  $('#selector').click(function() {
     $('.seleccionar__bottom').toggleClass('open');
    });

    $("#mouse-scroll").click(function(){
      // alert("hola");
      var x=  $('.descripcion').offset().top + -90;
      $('html, body').animate({
        scrollTop: x
      }, 1500);
  });

  $('.owl-modelo').owlCarousel({
    loop:true,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
  $('.owl-grid').owlCarousel({
   items:1,
   margin:10,
   autoHeight:true
})
});


$("a[section]").click(function() {
    // var top_ =
    $('#menu_').find('ul').find('li').find('a').removeClass('active')
     $('a[section="'+$(this).attr('section')+'"]').addClass('active')
     var x=  $('section[id="'+$(this).attr('section')+'"]').offset().top + -89;
     $('html, body').animate({
       scrollTop: x
     }, 1500);

})

$("a[sectionB]").click(function() {
     var x=  $('section[id="'+$(this).attr('sectionB')+'"]').offset().top + -89;
     $('html, body').animate({
       scrollTop: x
     }, 1500);

     $("#hamburger").click()
})



$(window).on('scroll', function () {
 var sections = $('section')
  , nav = $('nav')
  , nav_height = nav.outerHeight()
  , cur_pos = $(this).scrollTop();
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();

    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('li a').removeClass('active');
      sections.removeClass('active');

      $(this).addClass('active');
      nav.find('a[section="'+$(this).attr('id')+'"]').addClass('active');
    }
  });

});
