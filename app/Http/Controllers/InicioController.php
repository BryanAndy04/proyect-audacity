<?php

namespace App\Http\Controllers;

use App\Inicio;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Inicio::where('estado', '=', '1')->orderBy('orden', 'asc')->get();
        return view('administrable.inicio', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ( $request->updtae_ == '1' ) {


            Inicio::where('id', $request->inicioid)->update(
                                                              [
                                                                  'title' => trim(strtoupper($request->title)),
                                                                  'title_personalizado' => trim(strtoupper($request->title_personalizado)),
                                                                  'tipo_subtitulo' => trim($request->tipo_subtitulo),
                                                                  'subtitulo' => trim($request->subtitulo),
                                                                  'orden' => trim(strtoupper($request->orden))
                                                              ]
                                                            );



            return response()->json(['status' => "success", 'inicioid' => $request->inicioid]);

        }


        try {
            $inicio = new Inicio;
            $inicio->title = trim(strtoupper($request->title));
            $inicio->title_personalizado = trim(strtoupper($request->title_personalizado));
            $inicio->tipo_subtitulo = trim($request->tipo_subtitulo);
            $inicio->subtitulo = trim($request->subtitulo);
            $inicio->ruta_imagen = '/-/';
            $inicio->orden = $request->orden;
            $inicio->save();
            $inicio_id = $inicio->id; // this give us the last inserted record id
        } catch (\Exception $e) {
            return response()->json(['status' => 'exception', 'msg' => $e->getMessage()]);
        }
        return response()->json(['status' => "success", 'inicioid' => $inicio_id]);

    }

    public function iamgen(Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.' . $img->getClientOriginalExtension();
            $original_name = $img->getClientOriginalName();
            $path = Storage::putFileAs('public/inicio', $img, $imageName);
            Inicio::where('id', $request->inicioid)->update(['ruta_imagen' => 'inicio/' . $imageName]);
            return response()->json(['status' => "success", 'imgdata' => $original_name, 'inicioid' => $request->inicioid]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inicio  $inicio
     * @return \Illuminate\Http\Response
     */
    public function show(Inicio $inicio)
    {
        return response()->json(['status' => "success", 'data' => $inicio]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inicio  $inicio
     * @return \Illuminate\Http\Response
     */
    public function edit(Inicio $inicio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inicio  $inicio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inicio $inicio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inicio  $inicio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Inicio $inicio)
    {
        Inicio::where('id', $inicio->id)->update(['estado' => '0']);
        return response()->json(['status' => "success", 'data_delete' => $inicio]);
    }
}
