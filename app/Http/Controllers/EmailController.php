<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use PHPMailer\PHPMailer\PHPMailer;

class EmailController extends Controller
{

    public function envia_email(Request $request)
    {




        $data_template = array(

            'nombre' => $request->nombreapellido,
            'apellidos' => $request->nombreapellido,
            'tipodocumento' => $request->tipodocumento,
            'numerodocumento' => $request->numerodocumento,
            'email' => $request->email,
            'telefono' => $request->numerotelefonico,
            'publi_promociones' => $request->publi_promociones,
            'info_promociones' => $request->info_promociones,
            'tipodepartamento' => $request->tipodepartamento,
        );


        $view = View::make('audacity.email.template', [
            'data' => $data_template,
        ]);
        $mail = new PHPMailer(true);


        $mail->setFrom('contacto@audacity.pe', 'Asesórate Audacity');



        //$mail->addCC('audacity_senda@evolta.pe');


        $mail->addAddress($request->email, $request->nombreapellido);
        $mail->isHTML(true);
        $mail->Subject = 'Asesórate Audacity';
        $mail->Body = $view->render();
        $mail->AltBody = '-------';
        $mail->send();

        return view('audacity.gracias');

    }

}
