<?php

namespace App\Http\Controllers;

use App\Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galeria = Galeria::orderBy('orden','asc')->get();
        return view('administrable.galeria', ['galeria' => $galeria]);
    }

    public function position(Request $request)
    {
        $param_json = $request->params;
        $param_json = json_decode($param_json);


        foreach ($param_json->data as $row )
        {
            Galeria::where('id', $row->id)->update([ 'position' => $row->style, 'orden' => $row->orden  ]);

        }
        return response()->json(['status' => "success"]);
    }


    public function imagen(Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.' . $img->getClientOriginalExtension();
            $original_name = $img->getClientOriginalName();
            $path = Storage::putFileAs('public/galeria', $img, $imageName);
            Galeria::where('id', $request->id_galeria)->update(['link_imagen' => 'galeria/' . $imageName, 'orden' => '999']);
            return response()->json(['status' => "success", 'imgdata' => $original_name, 'id_galeria' => $request->id_galeria]);
        }
    }


    public function recorte_image(Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.jpg';

            if ( Storage::putFileAs('public/galeria', $img, $imageName)) {
                Galeria::where('id', $request->id_galeria)->update(['link_imagen_cropped' => 'galeria/' . $imageName]);
                return response()->json(['status' => "success"]);
            } else {
                return response()->json(['status' =>'error']);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $galeria  = new Galeria;
         $galeria->link_imagen = "/-/";
         $galeria->categoria = $request->categoria;
         $galeria->save();
         return response()->json(['status' => "success", 'id_galeria' => $galeria->id]);
    }

    public function destroy(Request $request,Galeria $galeria)
    {
        $galeria_ = Galeria::findOrFail($request->id);
        $galeria_->delete();
        return response()->json(['status' => "success", 'data_destroy' => $galeria_]);
    }
}
