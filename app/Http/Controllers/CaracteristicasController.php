<?php

namespace App\Http\Controllers;

use App\Caracteristicas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CaracteristicasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $caracteristicas = Caracteristicas::first();
        return view('administrable.caracteristicas', ['caracteristicas' => $caracteristicas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function imagen (Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.' . $img->getClientOriginalExtension();
            $original_name = $img->getClientOriginalName();
            $path = Storage::putFileAs('public/brochure', $img, $imageName);
            Caracteristicas::where('id','1')->update(['brochure' => 'brochure/' . $imageName]);
            return response()->json(['status' => "success", 'imgdata' => $original_name]);
        }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         return response()->json(['status' => "success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Caracteristicas  $caracteristicas
     * @return \Illuminate\Http\Response
     */
    public function show(Caracteristicas $caracteristicas)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Caracteristicas  $caracteristicas
     * @return \Illuminate\Http\Response
     */
    public function edit(Caracteristicas $caracteristicas)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Caracteristicas  $caracteristicas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Caracteristicas $caracteristicas)
    {
        Caracteristicas::where('id','1')->update(['vistapanoramica' => $request->vistapanoramica ]);
        return response()->json(['status' => "success"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Caracteristicas  $caracteristicas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Caracteristicas $caracteristicas)
    {
        //
    }
}
