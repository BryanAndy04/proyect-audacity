<?php

namespace App\Http\Controllers;

use App\Descripcion;
use Illuminate\Http\Request;

class DescripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descripcion = Descripcion::first();
        return view('administrable.descripcion', [ 'descripcion' => $descripcion ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Descripcion::where('id', 1)->update(
                                                [
                                                    'link_video' => ( $request->url_youtube ) ? $request->url_youtube : '<iframe width="560" height="315" src="https://www.youtube.com/embed/IJQKs6LdxYs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' ,
                                                    'exp_loft_url' => ( $request->url_loft )  ? $request->url_loft : 'https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/' ,
                                                    'exp_duo_url' => ( $request->exp_duo_url ) ? $request->exp_duo_url : '-',
                                                    'exp_social_url' => ( $request->exp_social_url ) ? $request->exp_social_url : '-',
                                                    'exp_area_com_url' => ( $request->exp_area_com_url ) ? $request->exp_area_com_url : '-'
                                                ]
                                            );
        return response()->json(['status' => "success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Descripcion  $descripcion
     * @return \Illuminate\Http\Response
     */
    public function show(Descripcion $descripcion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Descripcion  $descripcion
     * @return \Illuminate\Http\Response
     */
    public function edit(Descripcion $descripcion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Descripcion  $descripcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Descripcion $descripcion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Descripcion  $descripcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Descripcion $descripcion)
    {
        //
    }
}
