<?php

namespace App\Http\Controllers;

use App\Departamento;
use App\Planos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $loft = Departamento::where('categoria', 'loft')->where('estado', '1')->get();
        $duo = Departamento::where('categoria', 'duo')->where('estado', '1')->get();
        $social = Departamento::where('categoria', 'social')->where('estado', '1')->get();
        $unico = Departamento::where('categoria', 'unico')->where('estado', '1')->get();
        return view('administrable.departamentos', [
            'loft' => $loft,
            'duo' => $duo,
            'social' => $social,
            'unico' => $unico,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->updtae_ == '1') {

            Departamento::where('id', $request->inicioid)->update(
                [
                    'categoria' => trim($request->categoria),
                    'tipo' => trim(strtoupper($request->tipo)),
                    'area_total' => trim($request->area_total),
                    'habitaciones' => trim($request->habitaciones),
                    'banos' => trim($request->banos),
                ]
            );

            return response()->json(['status' => "success", 'inicioid' => $request->inicioid]);

        }

        try {
            $inicio = new Departamento;
            $inicio->img_principal = '/-/';
            $inicio->categoria = trim($request->categoria);
            $inicio->tipo = trim(strtoupper($request->tipo));
            $inicio->area_total = trim($request->area_total);
            $inicio->habitaciones = trim($request->habitaciones);
            $inicio->banos = trim($request->banos);
            $inicio->save();
            $inicio_id = $inicio->id; // this give us the last inserted record id
        } catch (\Exception $e) {
            return response()->json(['status' => 'exception', 'msg' => $e->getMessage()]);
        }
        return response()->json(['status' => "success", 'inicioid' => $inicio_id]);
    }

    public function imagen(Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.' . $img->getClientOriginalExtension();
            $original_name = $img->getClientOriginalName();
            $path = Storage::putFileAs('public/departamentos', $img, $imageName);
            Departamento::where('id', $request->inicioid)->update(['img_principal' => 'departamentos/' . $imageName]);
            return response()->json(['status' => "success", 'imgdata' => $original_name, 'inicioid' => $request->inicioid]);
        }
    }

    public function planostore(Request $request)
    {

        $plano = new Planos;
        $plano->id_departamento = $request->inicioid_;
        $plano->url_imagen = '-----------';
        $plano->save();
        $inicio_id = $plano->id;
        return response()->json(['status' => "success", 'inicioid' => $inicio_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function show(Departamento $departamento)
    {
        $data = [];
        $data['tipodepartamento'] = $departamento;
        $data['img_planos'] = Planos::where('id_departamento', $departamento->id)->orderBy('created_at', 'DESC')->get();

        return response()->json(['status' => "success", 'data' => $data]);
    }

    public function planosimage(Request $request)
    {
        if ($request->file('file')) {
            $img = $request->file('file');
            $imageName = strtotime(now()) . rand(11111, 99999) . '.' . $img->getClientOriginalExtension();
            $original_name = $img->getClientOriginalName();
            $path = Storage::putFileAs('public/departamentos/planos', $img, $imageName);
            Planos::where('id', $request->imagenid_)->update(['url_imagen' => 'departamentos/planos/' . $imageName]);
            return response()->json(['status' => "success", 'imgdata' => $original_name, 'inicioid' => $request->imagenid_]);
        }
    }

    public function planosdeleteimagen(Request $request)
    {
        $planos = Planos::findOrFail($request->id);
        $planos->delete();
        return response()->json(['status' => "success"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Departamento $departamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departamento $departamento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Departamento $departamento)
    {
        Departamento::where('id', $departamento->id)->update(['estado' => '0']);
        return response()->json(['status' => "success", 'data_delete' => $departamento]);
    }
}
