<?php

namespace App\Http\Controllers;
use App\Transparencia;
use Illuminate\Http\Request;

class TransparenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transparencia = Transparencia::first();
        return view('administrable.transparencia',['transparencia' => $transparencia]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         Transparencia::where('id',1)->update(['descripcion' => $request->descripcion]);
         return response()->json(['status' => "success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transparencia  $transparencia
     * @return \Illuminate\Http\Response
     */
    public function show(Transparencia $transparencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transparencia  $transparencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Transparencia $transparencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transparencia  $transparencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transparencia $transparencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transparencia  $transparencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transparencia $transparencia)
    {
        //
    }
}
