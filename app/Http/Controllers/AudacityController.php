<?php

namespace App\Http\Controllers;

use App\Caracteristicas;
use App\Descripcion;
use App\Galeria;
use App\Inicio;
use App\Transparencia;
use App\Departamento;
use Illuminate\Http\Request;

class AudacityController extends Controller
{

    public function index()
    {
        $slider = Inicio::where('estado', '=', '1')->orderBy('orden', 'asc')->get();
        $descripcion = Descripcion::first();
        $caracteristicas = Caracteristicas::first();
        $galeria = Galeria::orderBy('orden', 'asc')->get();
        $transparencia = Transparencia::first();
        $tipos_departamentos = Departamento::where('estado','1')->orderBy('categoria','ASC')->get();

        $contactos = array(
                            [
                               'titulo' => 'Asesora de ventas',
                               'nombre' => 'Sandra Ríos',
                               'num_wa' => '51970677013',
                               'icom__' => 'landing/img/iconos/woman.svg'
                            ],
                            [
                                'titulo' => 'Asesor de ventas',
                                'nombre' => 'Jonathan Ramos',
                                'num_wa' => '51998437883',
                                'icom__' => 'landing/img/iconos/men.svg'
                            ]
         );





        return view('audacity.app',
            [
                'slider'                => $slider,
                'descripcion'           => $descripcion,
                'caracteristicas'       => $caracteristicas,
                'galeria'               => $galeria,
                'transparencia'         => $transparencia,
                'tipos_departamentos'   => $tipos_departamentos,
                'contactos'             => $this->random($contactos,2)
            ]
        );

    }


    public function carga_tipo(Request $request)
    {
        $tipo = Departamento::where('categoria',$request->categoria)->where('estado','1')->get();
        return response()->json(['status' => "success", 'data' => $tipo  ]);
    }

    public function carga_galeria()
    {
        $galeria = Galeria::orderBy('orden', 'asc')->skip(6)->take(100)->get();
        return response()->json(['status' => "success", 'data' => $galeria  ]);
    }

    function random($arr, $num = 1) {
        shuffle($arr);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[] = $arr[$i];
        }
        return $num == 1 ? $r[0] : $r;
    }

}
