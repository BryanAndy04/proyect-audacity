<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inicio extends Model
{
    protected $table = 'inicio';
    protected $fillable = [
        'title', 'title_personalizado', 'tipo_subtitulo','subtitulo','ruta_imagen'
    ];


}
