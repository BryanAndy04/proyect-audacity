<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Inicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inicio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',255)->notnull();
            $table->string('title_personalizado',150)->notnull();
            $table->string('tipo_subtitulo',50);
            $table->string('subtitulo',255);
            $table->string('ruta_imagen',255)->notnull();
            $table->string('orden',25)->notnull();
            $table->char('estado',1)->default('1')->notnull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
