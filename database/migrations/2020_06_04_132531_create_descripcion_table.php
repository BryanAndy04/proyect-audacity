<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descripcion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link_video',255)->notnull();
            $table->string('exp_loft_url',255)->notnull();
            $table->string('exp_duo_url',255)->notnull();
            $table->string('exp_social_url',255);
            $table->string('exp_area_com_url',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descripcion');
    }
}
