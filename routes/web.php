<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'AudacityController@index');

Route::get('/img', 'AudacityController@carga_galeria');
Route::post('/galeriapostion', 'GaleriaController@position');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::post('/recorteimage','GaleriaController@recorte_image');
Route::resource('descripcion', 'DescripcionController');
Route::resource('inicio', 'InicioController');
Route::resource('caracteristicas', 'CaracteristicasController');
Route::resource('galeria','GaleriaController');
Route::resource('departamentos','DepartamentosController');
Route::resource('estado','EstadoController');
Route::post('departamentos/imagen/planos/store', 'DepartamentosController@planostore' )->name('departamentos.store.planos');
Route::post('departamentos/imagen/planos/imagen', 'DepartamentosController@planosimage' )->name('departamentos.imagen.planos');
Route::post('departamentos/imagen/planos/delete/imagen', 'DepartamentosController@planosdeleteimagen')->name('departamentos.imagen.delete.planos');
Route::get('departamentos/data/tipo','AudacityController@carga_tipo');
Route::post('asesorate/confirmacion','EmailController@envia_email');
Route::resource('transparencia','TransparenciaController');
Route::post('inicio/imagen', 'InicioController@iamgen')->name('inicio.imagen');
Route::post('caracteristicas/imagen', 'CaracteristicasController@imagen')->name('caracteristicas.imagen');
Route::post('galeria/imagen','GaleriaController@imagen')->name('galeria.imagen');
Route::post('departamentos/imagen','DepartamentosController@imagen')->name('departamentos.imagen');




